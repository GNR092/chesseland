﻿using Chesseland.db; // Importa el namespace Chesseland.db para acceder a la capa de datos
using Chesseland.formsaux; // Importa el namespace Chesseland.formsaux para acceder a otros formularios auxiliares
using Chesseland.init; // Importa el namespace Chesseland.init para acceder a la configuración inicial
using MySql.Data.MySqlClient; // Importa la clase MySqlConnection del conector MySQL
using System.ComponentModel; // Importa el namespace System.ComponentModel para trabajar con componentes

namespace Chesseland.login
{
    /// <summary>
    /// Formulario de inicio de sesión para gestionar el acceso al sistema.
    /// </summary>
    public partial class Login : Form
    {
        private bool _move = false; // Bandera para controlar el movimiento de la ventana
        private int mx, my; // Variables para almacenar las coordenadas del ratón

        /// <summary>
        /// Constructor de la clase Login.
        /// Realiza la configuración inicial del formulario y establece la conexión a la base de datos.
        /// </summary>
        public Login()
        {
            InitializeComponent(); // Inicializa los componentes del formulario

            // Verifica si existe el archivo de configuración de conexión a la base de datos
            if (!File.Exists("ConnectionData.cnf"))
            {
                // Si no existe, muestra el formulario de registro y espera a que se complete
                var reg = new Registro();
                reg.ShowDialog();

                // Si el registro se completó exitosamente, inicializa el formulario de inicio de sesión
                if (Helpers.code == 0)
                {
                    Environment.Exit(0); // Sale del programa si se cancela el registro
                }
                else
                {
                    Helpers.login = true; // Establece la bandera de inicio de sesión como verdadera
                }
            }
            else
            {
                // Si existe el archivo de configuración, carga los datos de conexión y los inicializa
                Helpers.Cnf = new Config();
                Helpers.Cnf.DesencryptConnectionData(); // Desencripta los datos de conexión
                Helpers.DB = new MySqlConnection();
                Helpers.DB.ConnectionString = string.Format("server={0};user={1};database={2};port={3};password={4};",
                                                             Helpers.Cnf.Host,
                                                             Helpers.Cnf.Username,
                                                             Helpers.Cnf.Database,
                                                             Helpers.Cnf.Port,
                                                             Helpers.Cnf.Password);

                // Inicializa los gestores de entidades para usuarios, productos y promociones
                Helpers.users = new UserManager(Helpers.DB);
                Helpers.products = new ProductManager(Helpers.DB);
                Helpers.promos = new PromoManager(Helpers.DB);
            }

            // Configuración del worker para realizar tareas en segundo plano
            Helpers.worker = new BackgroundWorker();
            Helpers.worker.DoWork += Worker_DoWork; // Evento para realizar el trabajo en segundo plano
            Helpers.worker.RunWorkerCompleted += Worker_RunWorkerCompleted; // Evento al completar el trabajo en segundo plano
        }

        /// <summary>
        /// Maneja el evento al completar el trabajo en segundo plano.
        /// Limpia el campo de contraseña y muestra el formulario de inicio de sesión.
        /// </summary>
        private void Worker_RunWorkerCompleted(object? sender, RunWorkerCompletedEventArgs e)
        {
            txt_passwd.Clear(); // Limpia el campo de contraseña
            Show(); // Muestra el formulario de inicio de sesión
        }

        /// <summary>
        /// Realiza el trabajo en segundo plano mientras el usuario está logueado.
        /// </summary>
        private void Worker_DoWork(object? sender, DoWorkEventArgs e)
        {
            // Ciclo que espera mientras el usuario está logueado
            while (Helpers.login)
            {
                Hide(); // Oculta el formulario de inicio de sesión
                Thread.Sleep(1000); // Espera 1 segundo
            }
        }

        /// <summary>
        /// Maneja el evento de clic en el botón de salida.
        /// Cierra la aplicación al salir.
        /// </summary>
        private void btn_exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0); // Cierra la aplicación
        }

        /// <summary>
        /// Maneja el evento de clic en el botón de ingreso.
        /// Verifica las credenciales del usuario y permite el acceso al sistema.
        /// </summary>
        private void btn_ingress_Click(object sender, EventArgs e)
        {
            // Obtiene el usuario por su nombre de usuario
            var user = Helpers.users?.GetUserByName(txt_user.Text);

            // Verifica si el usuario no existe o la contraseña no coincide
            if (user == null || !user.VerifyPassword(txt_passwd.Text))
            {
                MessageBox.Show("Usuario/Contraseña incorrecto"); // Muestra un mensaje de error
            }
            else
            {
                // Si las credenciales son correctas, establece el usuario actual y muestra el formulario principal
                Helpers.CurrentUser = user;
                IniciarSession();
                Hide(); // Oculta el formulario de inicio de sesión
            }
        }

        /// <summary>
        /// Método para iniciar la sesión del usuario y mostrar el formulario principal.
        /// </summary>
        private void IniciarSession()
        {
            Main main = new Main();
            main.Show(); // Muestra el formulario principal
            Helpers.login = true; // Establece la bandera de inicio de sesión como verdadera
            Helpers.worker?.RunWorkerAsync(); // Inicia el worker para realizar tareas en segundo plano
        }

        #region Mover ventana
        /// <summary>
        /// Maneja el evento al presionar el botón del ratón dentro del formulario.
        /// Habilita el movimiento de la ventana al arrastrar.
        /// </summary>
        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            _move = true; // Habilita el movimiento de la ventana
            mx = e.X; my = e.Y; // Obtiene las coordenadas del ratón
        }

        /// <summary>
        /// Maneja el evento al mover el ratón dentro del formulario.
        /// Mueve la ventana si está habilitado el movimiento.
        /// </summary>
        private void Login_MouseMove(object sender, MouseEventArgs e)
        {
            if (_move) SetDesktopLocation(MousePosition.X - mx, MousePosition.Y - my); // Mueve la ventana
        }

        /// <summary>
        /// Maneja el evento al soltar el botón del ratón dentro del formulario.
        /// Deshabilita el movimiento de la ventana.
        /// </summary>
        private void Login_MouseUp(object sender, MouseEventArgs e)
        {
            _move = false; // Deshabilita el movimiento de la ventana
        }
        #endregion

        /// <summary>
        /// Maneja el evento al cambiar el estado del checkbox para mostrar la contraseña.
        /// Habilita o deshabilita la visualización de la contraseña según el estado del checkbox.
        /// </summary>
        private void chk_mostraspasswd_CheckedChanged(object sender, EventArgs e)
        {
            txt_passwd.UseSystemPasswordChar = !chk_mostraspasswd.Checked; // Habilita o deshabilita la visualización de la contraseña
        }

        /// <summary>
        /// Maneja el evento al presionar una tecla dentro del campo de usuario.
        /// Enfoca el siguiente control al presionar la tecla Enter.
        /// </summary>
        private void txt_user_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true; // Maneja el evento de la tecla Enter
                SendKeys.Send("{TAB}"); // Enfoca el siguiente control (campo de contraseña)
            }
        }

        /// <summary>
        /// Maneja el evento al presionar una tecla dentro del campo de contraseña.
        /// Inicia el proceso de ingreso al sistema al presionar la tecla Enter.
        /// </summary>
        private void txt_passwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true; // Maneja el evento de la tecla Enter
                btn_ingress_Click(sender, e); // Ejecuta el evento de clic en el botón de ingreso
            }
        }

        /// <summary>
        /// Maneja el evento al hacer clic en el enlace para recuperar contraseña.
        /// Muestra el formulario de recuperación de contraseña.
        /// </summary>
        private void linkl_olvide_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var olvdc = new OlvideContra();
            olvdc.ShowDialog(); // Muestra el formulario de recuperación de contraseña
        }

        /// <summary>
        /// Maneja el evento de carga del formulario.
        /// Para el modo DEBUG, intenta autenticar automáticamente con el usuario "admin".
        /// </summary>
        private void Login_Load(object sender, EventArgs e)
        {
#if DEBUG
            var user = Helpers.users?.GetUserByName("admin");
            if (user == null)
            {
                MessageBox.Show("Usuario/Contraseña incorrecto"); // Muestra un mensaje de error
            }
            else
            {
                // Si las credenciales son correctas, establece el usuario actual y muestra el formulario principal
                Helpers.CurrentUser = user;
                IniciarSession();
                Hide(); // Oculta el formulario de inicio de sesión
            }
#endif
        }
    }
}