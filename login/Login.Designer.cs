﻿namespace Chesseland.login
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            txt_user = new TextBox();
            txt_passwd = new TextBox();
            btn_ingress = new Button();
            btn_exit = new Button();
            linkl_olvide = new LinkLabel();
            chk_mostraspasswd = new CheckBox();
            label4 = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Roboto Black", 22F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(44, 59);
            label1.Name = "label1";
            label1.Size = new Size(238, 37);
            label1.TabIndex = 0;
            label1.Text = "INICIAR SESIÓN";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(54, 140);
            label2.Name = "label2";
            label2.Size = new Size(50, 15);
            label2.TabIndex = 1;
            label2.Text = "Usuario:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(54, 212);
            label3.Name = "label3";
            label3.Size = new Size(70, 15);
            label3.TabIndex = 2;
            label3.Text = "Contraseña:";
            // 
            // txt_user
            // 
            txt_user.Location = new Point(54, 177);
            txt_user.Margin = new Padding(3, 2, 3, 2);
            txt_user.Name = "txt_user";
            txt_user.Size = new Size(240, 23);
            txt_user.TabIndex = 3;
            txt_user.KeyPress += txt_user_KeyPress;
            // 
            // txt_passwd
            // 
            txt_passwd.Location = new Point(54, 247);
            txt_passwd.Margin = new Padding(3, 2, 3, 2);
            txt_passwd.Name = "txt_passwd";
            txt_passwd.Size = new Size(240, 23);
            txt_passwd.TabIndex = 4;
            txt_passwd.UseSystemPasswordChar = true;
            txt_passwd.KeyPress += txt_passwd_KeyPress;
            // 
            // btn_ingress
            // 
            btn_ingress.BackColor = Color.CornflowerBlue;
            btn_ingress.FlatStyle = FlatStyle.Flat;
            btn_ingress.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_ingress.Location = new Point(83, 346);
            btn_ingress.Margin = new Padding(3, 2, 3, 2);
            btn_ingress.Name = "btn_ingress";
            btn_ingress.Size = new Size(164, 22);
            btn_ingress.TabIndex = 5;
            btn_ingress.Text = "INGRESAR";
            btn_ingress.UseVisualStyleBackColor = false;
            btn_ingress.Click += btn_ingress_Click;
            // 
            // btn_exit
            // 
            btn_exit.BackColor = Color.Crimson;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.Location = new Point(312, 11);
            btn_exit.Margin = new Padding(3, 2, 3, 2);
            btn_exit.Name = "btn_exit";
            btn_exit.Size = new Size(21, 22);
            btn_exit.TabIndex = 6;
            btn_exit.Text = "X";
            btn_exit.UseVisualStyleBackColor = false;
            btn_exit.Click += btn_exit_Click;
            // 
            // linkl_olvide
            // 
            linkl_olvide.AutoSize = true;
            linkl_olvide.Location = new Point(186, 283);
            linkl_olvide.Name = "linkl_olvide";
            linkl_olvide.Size = new Size(119, 15);
            linkl_olvide.TabIndex = 8;
            linkl_olvide.TabStop = true;
            linkl_olvide.Text = "Olvidé mi contraseña";
            linkl_olvide.LinkClicked += linkl_olvide_LinkClicked;
            // 
            // chk_mostraspasswd
            // 
            chk_mostraspasswd.AutoSize = true;
            chk_mostraspasswd.Location = new Point(52, 283);
            chk_mostraspasswd.Margin = new Padding(3, 2, 3, 2);
            chk_mostraspasswd.Name = "chk_mostraspasswd";
            chk_mostraspasswd.Size = new Size(128, 19);
            chk_mostraspasswd.TabIndex = 10;
            chk_mostraspasswd.Text = "Mostrar contraseña";
            chk_mostraspasswd.UseVisualStyleBackColor = true;
            chk_mostraspasswd.CheckedChanged += chk_mostraspasswd_CheckedChanged;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(287, 6);
            label4.Name = "label4";
            label4.Size = new Size(18, 27);
            label4.TabIndex = 11;
            label4.Text = "-";
            // 
            // Login
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(345, 406);
            Controls.Add(label4);
            Controls.Add(chk_mostraspasswd);
            Controls.Add(linkl_olvide);
            Controls.Add(btn_exit);
            Controls.Add(btn_ingress);
            Controls.Add(txt_passwd);
            Controls.Add(txt_user);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            FormBorderStyle = FormBorderStyle.None;
            Margin = new Padding(3, 2, 3, 2);
            Name = "Login";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Login";
            Load += Login_Load;
            MouseDown += Login_MouseDown;
            MouseMove += Login_MouseMove;
            MouseUp += Login_MouseUp;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox txt_user;
        private TextBox txt_passwd;
        private Button btn_ingress;
        private Button btn_exit;
        private LinkLabel linkl_olvide;
        private CheckBox chk_mostraspasswd;
        private Label label4;
    }
}