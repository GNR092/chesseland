﻿namespace Chesseland.formsaux
{
    /// <summary>
    /// AddIngredient
    /// </summary>
    public partial class AddIngredient : Form
    {
        /// <summary>
        /// contructor de la clase AddIngredient
        /// </summary>
        public AddIngredient()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de agregar ingrediente
        private void btn_add_Click(object sender, EventArgs e)
        {
            // Verifica si la lista temporal de ingredientes es nula, y si es así, la inicializa
            if (Helpers.TempIngds == null) Helpers.TempIngds = new();

            // Verifica si el campo de selección de ingredientes está vacío
            if (string.IsNullOrEmpty(cmb_ing.Text))
            {
                lb_info.Text = "Seleccione un ingrediente."; // Muestra un mensaje indicando que se debe seleccionar un ingrediente
                return; // Sale del método sin realizar más acciones
            }

            Helpers.TempIngds.Add(cmb_ing.Text); // Agrega el ingrediente seleccionado a la lista temporal de ingredientes
            Close(); // Cierra el formulario
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de selección de ingredientes
        private void cmb_ing_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verifica si se presionó la tecla Enter
            if (e.KeyChar.IsKeyEnterPress())
            {
                e.Handled = true; // Indica que se ha manejado el evento de la tecla presionada
                btn_add_Click(sender, e); // Llama al evento de clic del botón de agregar ingrediente
            }
        }
    }
}
