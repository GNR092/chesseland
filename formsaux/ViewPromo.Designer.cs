﻿namespace Chesseland.forms
{
    partial class ViewPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            datag_products = new DataGridView();
            c_nombre = new DataGridViewTextBoxColumn();
            c_precio = new DataGridViewTextBoxColumn();
            c_ings = new DataGridViewTextBoxColumn();
            btn_select = new Button();
            ((System.ComponentModel.ISupportInitialize)datag_products).BeginInit();
            SuspendLayout();
            // 
            // datag_products
            // 
            datag_products.AllowUserToAddRows = false;
            datag_products.AllowUserToDeleteRows = false;
            datag_products.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            datag_products.Columns.AddRange(new DataGridViewColumn[] { c_nombre, c_precio, c_ings });
            datag_products.Location = new Point(12, 30);
            datag_products.Name = "datag_products";
            datag_products.ReadOnly = true;
            datag_products.RowTemplate.Height = 25;
            datag_products.Size = new Size(498, 265);
            datag_products.TabIndex = 2;
            // 
            // c_nombre
            // 
            c_nombre.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_nombre.HeaderText = "Nombre";
            c_nombre.Name = "c_nombre";
            c_nombre.ReadOnly = true;
            c_nombre.Resizable = DataGridViewTriState.False;
            // 
            // c_precio
            // 
            c_precio.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_precio.HeaderText = "Precio";
            c_precio.Name = "c_precio";
            c_precio.ReadOnly = true;
            // 
            // c_ings
            // 
            c_ings.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_ings.HeaderText = "Ingredientes";
            c_ings.Name = "c_ings";
            c_ings.ReadOnly = true;
            // 
            // btn_select
            // 
            btn_select.Location = new Point(193, 334);
            btn_select.Name = "btn_select";
            btn_select.Size = new Size(104, 23);
            btn_select.TabIndex = 5;
            btn_select.Text = "Seleccionar";
            btn_select.UseVisualStyleBackColor = true;
            btn_select.Click += btn_select_Click;
            // 
            // ViewPromo
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(517, 369);
            Controls.Add(btn_select);
            Controls.Add(datag_products);
            Name = "ViewPromo";
            StartPosition = FormStartPosition.CenterParent;
            Text = "Promos";
            TopMost = true;
            Load += ViewPromo_Load;
            ((System.ComponentModel.ISupportInitialize)datag_products).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private DataGridView datag_products;
        private Button btn_select;
        private DataGridViewTextBoxColumn c_nombre;
        private DataGridViewTextBoxColumn c_precio;
        private DataGridViewTextBoxColumn c_ings;
    }
}