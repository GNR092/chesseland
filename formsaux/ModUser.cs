﻿using Chesseland.db; // Importa el namespace Chesseland.db

namespace Chesseland.forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ModUser : Form
    {
        private User? tmpuser; // Variable para almacenar temporalmente un usuario
        /// <summary>
        /// 
        /// </summary>
        public ModUser()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de modificar
        private void btn_mod_Click(object sender, EventArgs e)
        {
            // Muestra un cuadro de diálogo para confirmar si se desean hacer cambios
            if (MessageBox.Show("¿Quieres hacer cambios?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var user = new User(); // Crea una nueva instancia de User
                user.IdUser = tmpuser!.IdUser; // Asigna el ID del usuario temporal al nuevo usuario
                user.Username = txt_nombre.Text; // Asigna el nombre de usuario desde el campo de texto
                user.Rol = cmb_rol.Text.GetRol(); // Asigna el rol del usuario desde el combobox
                if (txt_contra.Text != "") user.CreateBCryptHash(txt_contra.Text); // Si la contraseña no está vacía, crea un hash de la contraseña
                user.Email = txt_correo.Text; // Asigna el correo electrónico desde el campo de texto
                try
                {
                    Helpers.users!.UpdateUser(user); // Intenta actualizar el usuario en la base de datos
                    MessageBox.Show("¡Usuario Actualizado!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information); // Muestra un mensaje de éxito
                }
                catch (Exception ex)
                {
                    // Captura cualquier excepción y muestra un mensaje de error
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Evento que se dispara cuando se carga el formulario
        private void ModUser_Load(object sender, EventArgs e)
        {
            tmpuser = DelUser.tempuser; // Obtiene el usuario temporal de la clase DelUser
            txt_nombre.Text = tmpuser!.Username; // Asigna el nombre de usuario al campo de texto
            cmb_rol.Text = tmpuser!.Rol.GetRolToString(); // Asigna el rol del usuario al combobox
            txt_correo.Text = tmpuser.Email; // Asigna el correo electrónico al campo de texto
        }
    }
}
