﻿using Chesseland.db; // Importa el namespace Chesseland.db

namespace Chesseland.forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ViewProducts : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public ViewProducts()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de buscar
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            if (chk_promo.Checked) // Verifica si el checkbox de promoción está marcado
            {
                var pm = new Promo(); // Crea una nueva instancia de Promo
                // Aquí se podría añadir lógica adicional para buscar promociones
            }
            else
            {
                var pds = Helpers.products?.SearchProduct(txt_name.Text); // Busca productos según el nombre introducido
                datag_products.Rows.Clear(); // Limpia las filas del DataGridView
                foreach (var p in pds!) // Recorre cada producto encontrado
                {
                    int index = datag_products.Rows.Add(); // Añade una nueva fila al DataGridView
                    datag_products.Rows[index].Cells[0].Value = p.Nombre; // Asigna el nombre del producto a la celda
                    datag_products.Rows[index].Cells[1].Value = p.Precio; // Asigna el precio del producto a la celda
                    datag_products.Rows[index].Cells[2].Value = p.Ingredientes; // Asigna los ingredientes del producto a la celda
                }
            }
        }

        // Evento que se dispara cuando se hace clic en el botón de seleccionar
        private void btn_select_Click(object sender, EventArgs e)
        {
            try
            {
                Ventas.tmpprodct = new(); // Inicializa un nuevo producto temporal
                Ventas.tmpprodct.Nombre = (string)GetDataValues(0); // Asigna el nombre del producto seleccionado
                Ventas.tmpprodct.Precio = (int)GetDataValues(1); // Asigna el precio del producto seleccionado
                Ventas.tmpprodct.Ingredientes = (string)GetDataValues(2); // Asigna los ingredientes del producto seleccionado
                Ventas.tmpprodct.Cantidad = int.Parse(txt_cantidad.Text); // Asigna la cantidad del producto basado en la entrada del usuario
            }
            catch
            {
                // Captura cualquier excepción que pueda ocurrir
            }
            Close(); // Cierra el formulario
        }

        // Método para obtener los valores de la celda en el DataGridView
        private object GetDataValues(int index)
        {
            DataGridViewCell a = datag_products.CurrentCell; // Obtiene la celda actual
            return datag_products.Rows[a.RowIndex].Cells[index].Value; // Devuelve el valor de la celda en la fila actual y el índice especificado
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de cantidad
        private void txt_cantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar.IsOnlyNums(); // Verifica si la tecla presionada es un número
            if (e.Handled) lb_cantidad.Text = "Solo se permiten numeros."; // Muestra un mensaje si no es un número
            else lb_cantidad.Text = ""; // Limpia el mensaje si es un número
        }
    }
}
