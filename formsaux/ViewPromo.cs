﻿using Chesseland.db; // Importa el namespace Chesseland.db

namespace Chesseland.forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ViewPromo : Form
    {
        private List<Promo>? Promos; // Lista para almacenar las promociones

        /// <summary>
        /// 
        /// </summary>
        public ViewPromo()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de seleccionar
        private void btn_select_Click(object sender, EventArgs e)
        {
            try
            {
                if (Helpers.temppromo == null) // Verifica si la promoción temporal es nula
                {
                    Helpers.temppromo = new Promo(); // Inicializa una nueva promoción temporal
                    var p = Promos!.Find(pp => pp.Nombpromo == (string)GetDataValues(0)); // Busca la promoción en la lista
                    if (p!.Nombpromo == (string)GetDataValues(0)) // Verifica si el nombre de la promoción coincide
                    {
                        Helpers.temppromo = p; // Asigna la promoción encontrada a la promoción temporal
                    }
                    Close(); // Cierra el formulario
                }
            }
            catch
            {
                // Captura cualquier excepción que pueda ocurrir
            }
        }

        // Método para obtener los valores de la celda en el DataGridView
        private object GetDataValues(int index)
        {
            DataGridViewCell a = datag_products.CurrentCell; // Obtiene la celda actual
            return datag_products.Rows[a.RowIndex].Cells[index].Value; // Devuelve el valor de la celda en la fila actual y el índice especificado
        }

        // Evento que se dispara cuando se carga el formulario
        private void ViewPromo_Load(object sender, EventArgs e)
        {
            Promos = Helpers.promos?.GetPromo(); // Obtiene la lista de promociones
            datag_products.Rows.Clear(); // Limpia las filas del DataGridView
            foreach (var p in Promos!) // Recorre cada promoción en la lista
            {
                int index = datag_products.Rows.Add(); // Añade una nueva fila al DataGridView
                datag_products.Rows[index].Cells[0].Value = p.Nombpromo; // Asigna el nombre de la promoción a la celda
                datag_products.Rows[index].Cells[1].Value = p.Preciopromo; // Asigna el precio de la promoción a la celda
                datag_products.Rows[index].Cells[2].Value = p.Ingredientes; // Asigna los ingredientes de la promoción a la celda
            }
        }
    }
}
