﻿namespace Chesseland.forms
{
    partial class AddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            txt_nombre = new TextBox();
            txt_contra = new TextBox();
            label2 = new Label();
            label3 = new Label();
            txt_correo = new TextBox();
            label4 = new Label();
            cmb_rol = new ComboBox();
            btn_add = new Button();
            btn_back = new Button();
            label5 = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(24, 77);
            label1.Name = "label1";
            label1.Size = new Size(51, 15);
            label1.TabIndex = 0;
            label1.Text = "Nombre";
            // 
            // txt_nombre
            // 
            txt_nombre.Location = new Point(95, 69);
            txt_nombre.Name = "txt_nombre";
            txt_nombre.Size = new Size(100, 23);
            txt_nombre.TabIndex = 1;
            // 
            // txt_contra
            // 
            txt_contra.Location = new Point(95, 98);
            txt_contra.Name = "txt_contra";
            txt_contra.Size = new Size(100, 23);
            txt_contra.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(24, 106);
            label2.Name = "label2";
            label2.Size = new Size(67, 15);
            label2.TabIndex = 2;
            label2.Text = "Contraseña";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(24, 135);
            label3.Name = "label3";
            label3.Size = new Size(24, 15);
            label3.TabIndex = 4;
            label3.Text = "Rol";
            // 
            // txt_correo
            // 
            txt_correo.Location = new Point(95, 156);
            txt_correo.Name = "txt_correo";
            txt_correo.Size = new Size(100, 23);
            txt_correo.TabIndex = 7;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(24, 164);
            label4.Name = "label4";
            label4.Size = new Size(43, 15);
            label4.TabIndex = 6;
            label4.Text = "Correo";
            // 
            // cmb_rol
            // 
            cmb_rol.DropDownStyle = ComboBoxStyle.DropDownList;
            cmb_rol.FormattingEnabled = true;
            cmb_rol.Items.AddRange(new object[] { "Superadmin", "Admin", "Cajero", "Mesero" });
            cmb_rol.Location = new Point(95, 132);
            cmb_rol.Name = "cmb_rol";
            cmb_rol.Size = new Size(121, 23);
            cmb_rol.TabIndex = 5;
            // 
            // btn_add
            // 
            btn_add.Location = new Point(63, 216);
            btn_add.Name = "btn_add";
            btn_add.Size = new Size(63, 27);
            btn_add.TabIndex = 8;
            btn_add.Text = "Agregar";
            btn_add.UseVisualStyleBackColor = true;
            btn_add.Click += btn_add_Click;
            // 
            // btn_back
            // 
            btn_back.Location = new Point(132, 216);
            btn_back.Name = "btn_back";
            btn_back.Size = new Size(63, 27);
            btn_back.TabIndex = 9;
            btn_back.Text = "Regesar";
            btn_back.UseVisualStyleBackColor = true;
            btn_back.Click += btn_back_Click;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Roboto Medium", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(63, 23);
            label5.Name = "label5";
            label5.Size = new Size(173, 17);
            label5.TabIndex = 10;
            label5.Text = "Agregar nuevo usuario";
            // 
            // AddUser
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(270, 292);
            Controls.Add(label5);
            Controls.Add(btn_back);
            Controls.Add(btn_add);
            Controls.Add(cmb_rol);
            Controls.Add(txt_correo);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(txt_contra);
            Controls.Add(label2);
            Controls.Add(txt_nombre);
            Controls.Add(label1);
            Name = "AddUser";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Agregar usuario";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private TextBox txt_nombre;
        private TextBox txt_contra;
        private Label label2;
        private Label label3;
        private TextBox txt_correo;
        private Label label4;
        private ComboBox cmb_rol;
        private Button btn_add;
        private Button btn_back;
        private Label label5;
    }
}