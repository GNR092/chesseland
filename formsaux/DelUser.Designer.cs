﻿namespace Chesseland.forms
{
    partial class DelUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            label5 = new Label();
            txt_nombre = new TextBox();
            label1 = new Label();
            btn_buscar = new Button();
            datagridusers = new DataGridView();
            clm_id = new DataGridViewTextBoxColumn();
            clm_nombre = new DataGridViewTextBoxColumn();
            clm_rol = new DataGridViewTextBoxColumn();
            clm_email = new DataGridViewTextBoxColumn();
            btn_mod = new Button();
            btn_elim = new Button();
            toolTip1 = new ToolTip(components);
            ((System.ComponentModel.ISupportInitialize)datagridusers).BeginInit();
            SuspendLayout();
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Roboto Medium", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(64, 31);
            label5.Name = "label5";
            label5.Size = new Size(213, 17);
            label5.TabIndex = 21;
            label5.Text = "Eliminar o modificar usuario";
            // 
            // txt_nombre
            // 
            txt_nombre.Location = new Point(96, 60);
            txt_nombre.Name = "txt_nombre";
            txt_nombre.Size = new Size(126, 23);
            txt_nombre.TabIndex = 12;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(39, 64);
            label1.Name = "label1";
            label1.Size = new Size(51, 15);
            label1.TabIndex = 11;
            label1.Text = "Nombre";
            // 
            // btn_buscar
            // 
            btn_buscar.Location = new Point(228, 64);
            btn_buscar.Name = "btn_buscar";
            btn_buscar.Size = new Size(75, 23);
            btn_buscar.TabIndex = 22;
            btn_buscar.Text = "Buscar";
            toolTip1.SetToolTip(btn_buscar, "Puedes dejar en blanco para motrar todos los usuarios disponibles");
            btn_buscar.UseVisualStyleBackColor = true;
            btn_buscar.Click += btn_buscar_Click;
            // 
            // datagridusers
            // 
            datagridusers.AllowUserToAddRows = false;
            datagridusers.AllowUserToDeleteRows = false;
            datagridusers.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            datagridusers.Columns.AddRange(new DataGridViewColumn[] { clm_id, clm_nombre, clm_rol, clm_email });
            datagridusers.Location = new Point(25, 104);
            datagridusers.Name = "datagridusers";
            datagridusers.ReadOnly = true;
            datagridusers.RowTemplate.Height = 25;
            datagridusers.Size = new Size(528, 150);
            datagridusers.TabIndex = 23;
            toolTip1.SetToolTip(datagridusers, "Selecciona un usuario para poder modificar o eliminar con los botones");
            // 
            // clm_id
            // 
            clm_id.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            clm_id.HeaderText = "ID";
            clm_id.Name = "clm_id";
            clm_id.ReadOnly = true;
            clm_id.Resizable = DataGridViewTriState.False;
            clm_id.Width = 43;
            // 
            // clm_nombre
            // 
            clm_nombre.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            clm_nombre.HeaderText = "Nombre";
            clm_nombre.Name = "clm_nombre";
            clm_nombre.ReadOnly = true;
            clm_nombre.Resizable = DataGridViewTriState.False;
            clm_nombre.Width = 76;
            // 
            // clm_rol
            // 
            clm_rol.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            clm_rol.HeaderText = "Rol";
            clm_rol.Name = "clm_rol";
            clm_rol.ReadOnly = true;
            clm_rol.Width = 49;
            // 
            // clm_email
            // 
            clm_email.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            clm_email.HeaderText = "Correo";
            clm_email.Name = "clm_email";
            clm_email.ReadOnly = true;
            // 
            // btn_mod
            // 
            btn_mod.Location = new Point(347, 318);
            btn_mod.Name = "btn_mod";
            btn_mod.Size = new Size(75, 23);
            btn_mod.TabIndex = 24;
            btn_mod.Text = "Modificar";
            btn_mod.UseVisualStyleBackColor = true;
            btn_mod.Click += btn_mod_Click;
            // 
            // btn_elim
            // 
            btn_elim.Location = new Point(428, 318);
            btn_elim.Name = "btn_elim";
            btn_elim.Size = new Size(75, 23);
            btn_elim.TabIndex = 25;
            btn_elim.Text = "Eliminar";
            btn_elim.UseVisualStyleBackColor = true;
            btn_elim.Click += btn_elim_Click;
            // 
            // DelUser
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(583, 368);
            Controls.Add(btn_elim);
            Controls.Add(btn_mod);
            Controls.Add(datagridusers);
            Controls.Add(btn_buscar);
            Controls.Add(label5);
            Controls.Add(txt_nombre);
            Controls.Add(label1);
            Name = "DelUser";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "DelUser";
            Load += DelUser_Load;
            ((System.ComponentModel.ISupportInitialize)datagridusers).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label5;
        private TextBox txt_nombre;
        private Label label1;
        private Button btn_buscar;
        private DataGridView datagridusers;
        private DataGridViewTextBoxColumn clm_id;
        private DataGridViewTextBoxColumn clm_nombre;
        private DataGridViewTextBoxColumn clm_rol;
        private DataGridViewTextBoxColumn clm_email;
        private Button btn_mod;
        private Button btn_elim;
        private ToolTip toolTip1;
    }
}