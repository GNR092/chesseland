﻿namespace Chesseland.formsaux
{
    /// <summary>
    /// 
    /// </summary>
    public partial class OlvideContra : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public OlvideContra()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón (button1)
        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox1.Text)) // Verifica que el campo de texto no esté vacío ni contenga solo espacios en blanco
            {
                if (!textBox1.Text.Contains("@")) // Verifica que el texto contenga el carácter '@'
                {
                    MessageBox.Show("El correo debe contener @"); // Muestra un mensaje si no contiene '@'
                }
                else
                {
                    MessageBox.Show($"Se ha enviado un enlace al correo {textBox1.Text} para la recuperación de contraseña."); // Muestra un mensaje de éxito
                    Close(); // Cierra el formulario
                }
            }
            else
            {
                MessageBox.Show("El campo no debe contener espacios o estar vacio."); // Muestra un mensaje si el campo está vacío o contiene solo espacios
            }
        }
    }
}
