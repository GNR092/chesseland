﻿namespace Chesseland.formsaux
{
    partial class AddIngredient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            cmb_ing = new ComboBox();
            btn_add = new Button();
            lb_info = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(25, 37);
            label1.Name = "label1";
            label1.Size = new Size(133, 15);
            label1.TabIndex = 0;
            label1.Text = "Seleccionar ingrediente:";
            // 
            // cmb_ing
            // 
            cmb_ing.DropDownStyle = ComboBoxStyle.DropDownList;
            cmb_ing.FlatStyle = FlatStyle.Popup;
            cmb_ing.FormattingEnabled = true;
            cmb_ing.Items.AddRange(new object[] { "Pepperoni", "Jamón", "Tocino", "Chorizo", "Longaniza", "Salchicha", "Champiñones", "Cebolla", "Pimiento", "Aceitunas negras", "Piña", "Queso", "Queso Extra" });
            cmb_ing.Location = new Point(25, 55);
            cmb_ing.Name = "cmb_ing";
            cmb_ing.Size = new Size(147, 23);
            cmb_ing.TabIndex = 1;
            cmb_ing.KeyPress += cmb_ing_KeyPress;
            // 
            // btn_add
            // 
            btn_add.Location = new Point(54, 107);
            btn_add.Name = "btn_add";
            btn_add.Size = new Size(75, 23);
            btn_add.TabIndex = 2;
            btn_add.Text = "Agregar";
            btn_add.UseVisualStyleBackColor = true;
            btn_add.Click += btn_add_Click;
            // 
            // lb_info
            // 
            lb_info.AutoSize = true;
            lb_info.Font = new Font("Consolas", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lb_info.ForeColor = Color.Red;
            lb_info.Location = new Point(8, 83);
            lb_info.Name = "lb_info";
            lb_info.Size = new Size(0, 14);
            lb_info.TabIndex = 29;
            // 
            // AddIngredient
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(191, 152);
            Controls.Add(lb_info);
            Controls.Add(btn_add);
            Controls.Add(cmb_ing);
            Controls.Add(label1);
            Name = "AddIngredient";
            StartPosition = FormStartPosition.CenterParent;
            Text = "AddIngredient";
            TopMost = true;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private ComboBox cmb_ing;
        private Button btn_add;
        private Label lb_info;
    }
}