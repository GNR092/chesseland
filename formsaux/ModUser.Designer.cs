﻿namespace Chesseland.forms
{
    partial class ModUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btn_back = new Button();
            btn_mod = new Button();
            cmb_rol = new ComboBox();
            txt_correo = new TextBox();
            label4 = new Label();
            label3 = new Label();
            txt_contra = new TextBox();
            label2 = new Label();
            txt_nombre = new TextBox();
            label1 = new Label();
            label5 = new Label();
            SuspendLayout();
            // 
            // btn_back
            // 
            btn_back.Location = new Point(162, 231);
            btn_back.Name = "btn_back";
            btn_back.Size = new Size(63, 27);
            btn_back.TabIndex = 19;
            btn_back.Text = "Regesar";
            btn_back.UseVisualStyleBackColor = true;
            // 
            // btn_mod
            // 
            btn_mod.Location = new Point(93, 231);
            btn_mod.Name = "btn_mod";
            btn_mod.Size = new Size(63, 27);
            btn_mod.TabIndex = 18;
            btn_mod.Text = "Modificar";
            btn_mod.UseVisualStyleBackColor = true;
            btn_mod.Click += btn_mod_Click;
            // 
            // cmb_rol
            // 
            cmb_rol.DropDownStyle = ComboBoxStyle.DropDownList;
            cmb_rol.FormattingEnabled = true;
            cmb_rol.Items.AddRange(new object[] { "Admin", "Cajero", "Mesero" });
            cmb_rol.Location = new Point(125, 147);
            cmb_rol.Name = "cmb_rol";
            cmb_rol.Size = new Size(121, 23);
            cmb_rol.TabIndex = 15;
            // 
            // txt_correo
            // 
            txt_correo.Location = new Point(125, 171);
            txt_correo.Name = "txt_correo";
            txt_correo.Size = new Size(100, 23);
            txt_correo.TabIndex = 17;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(54, 179);
            label4.Name = "label4";
            label4.Size = new Size(43, 15);
            label4.TabIndex = 16;
            label4.Text = "Correo";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(54, 150);
            label3.Name = "label3";
            label3.Size = new Size(24, 15);
            label3.TabIndex = 14;
            label3.Text = "Rol";
            // 
            // txt_contra
            // 
            txt_contra.Location = new Point(125, 113);
            txt_contra.Name = "txt_contra";
            txt_contra.Size = new Size(100, 23);
            txt_contra.TabIndex = 13;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(54, 121);
            label2.Name = "label2";
            label2.Size = new Size(67, 15);
            label2.TabIndex = 12;
            label2.Text = "Contraseña";
            // 
            // txt_nombre
            // 
            txt_nombre.Location = new Point(125, 84);
            txt_nombre.Name = "txt_nombre";
            txt_nombre.Size = new Size(100, 23);
            txt_nombre.TabIndex = 11;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(54, 92);
            label1.Name = "label1";
            label1.Size = new Size(51, 15);
            label1.TabIndex = 10;
            label1.Text = "Nombre";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Roboto Medium", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(77, 32);
            label5.Name = "label5";
            label5.Size = new Size(135, 17);
            label5.TabIndex = 20;
            label5.Text = "Modificar usuario";
            // 
            // ModUser
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(291, 293);
            Controls.Add(label5);
            Controls.Add(btn_back);
            Controls.Add(btn_mod);
            Controls.Add(cmb_rol);
            Controls.Add(txt_correo);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(txt_contra);
            Controls.Add(label2);
            Controls.Add(txt_nombre);
            Controls.Add(label1);
            Name = "ModUser";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "ModUser";
            Load += ModUser_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btn_back;
        private Button btn_mod;
        private ComboBox cmb_rol;
        private TextBox txt_correo;
        private Label label4;
        private Label label3;
        private TextBox txt_contra;
        private Label label2;
        private TextBox txt_nombre;
        private Label label1;
        private Label label5;
    }
}