﻿using Chesseland.db; // Importa el namespace Chesseland.db

namespace Chesseland.forms
{
    /// <summary>
    /// clase AddUser.
    /// </summary>
    public partial class AddUser : Form
    {
        /// <summary>
        /// Constructor público de la clase AddUser.
        /// </summary>
        public AddUser()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }


        // Evento que se dispara cuando se hace clic en el botón de agregar usuario
        private void btn_add_Click(object sender, EventArgs e)
        {
            // Verifica si alguno de los campos obligatorios está vacío
            if (string.IsNullOrEmpty(txt_nombre.Text) || string.IsNullOrEmpty(txt_correo.Text) || string.IsNullOrEmpty(txt_contra.Text))
            {
                // Muestra un mensaje de advertencia indicando que se deben rellenar todos los campos
                MessageBox.Show("Campos vacios rellene todos los campos.", "Campos vacios", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return; // Sale del método sin realizar más acciones
            }

            // Verifica si el campo de selección de rol está vacío
            if (string.IsNullOrEmpty(cmb_rol.Text))
            {
                // Muestra un mensaje de advertencia indicando que se debe seleccionar un rol
                MessageBox.Show("Seleccione un rol.", "Campos vacio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return; // Sale del método sin realizar más acciones
            }

            var user = new User(); // Crea una nueva instancia de User
            try
            {
                user.Username = txt_nombre.Text; // Asigna el nombre de usuario introducido
                user.CreateBCryptHash(txt_contra.Text); // Crea un hash BCrypt a partir de la contraseña introducida
                user.Rol = cmb_rol.Text.GetRol(); // Obtiene el rol seleccionado y lo asigna al usuario
                user.Email = txt_correo.Text; // Asigna el correo electrónico introducido

                Helpers.users!.AddUser(user); // Agrega el usuario utilizando el helper de usuarios
                MessageBox.Show("Guardado con exito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information); // Muestra un mensaje de éxito
                Clear(); // Llama al método Clear para limpiar los campos del formulario

            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); } // Captura y muestra cualquier excepción que ocurra
        }
        /// <summary>
        /// Método para limpiar los campos del formulario
        /// </summary>
        public void Clear()
        {
            foreach (Control control in Controls) // Recorre todos los controles del formulario
            {
                if (control is TextBox textBox) // Verifica si el control actual es un TextBox
                {
                    textBox.Clear(); // Limpia el contenido del TextBox
                }
            }
            cmb_rol.Text = string.Empty; // Limpia el campo de selección de rol
        }
        /// <summary>
        /// Evento que se dispara cuando se hace clic en el botón de regresar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_back_Click(object sender, EventArgs e)
        {
            Close(); // Cierra el formulario
        }
    }
}
