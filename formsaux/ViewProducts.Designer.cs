﻿namespace Chesseland.forms
{
    partial class ViewProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            txt_name = new TextBox();
            datag_products = new DataGridView();
            c_nombre = new DataGridViewTextBoxColumn();
            c_precio = new DataGridViewTextBoxColumn();
            c_ings = new DataGridViewTextBoxColumn();
            btn_buscar = new Button();
            chk_promo = new CheckBox();
            btn_select = new Button();
            txt_cantidad = new TextBox();
            label2 = new Label();
            lb_cantidad = new Label();
            ((System.ComponentModel.ISupportInitialize)datag_products).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(24, 49);
            label1.Name = "label1";
            label1.Size = new Size(103, 15);
            label1.TabIndex = 0;
            label1.Text = "Nombre producto";
            // 
            // txt_name
            // 
            txt_name.Location = new Point(131, 46);
            txt_name.Name = "txt_name";
            txt_name.Size = new Size(145, 23);
            txt_name.TabIndex = 1;
            // 
            // datag_products
            // 
            datag_products.AllowUserToAddRows = false;
            datag_products.AllowUserToDeleteRows = false;
            datag_products.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            datag_products.Columns.AddRange(new DataGridViewColumn[] { c_nombre, c_precio, c_ings });
            datag_products.Location = new Point(24, 98);
            datag_products.Name = "datag_products";
            datag_products.ReadOnly = true;
            datag_products.RowTemplate.Height = 25;
            datag_products.Size = new Size(498, 265);
            datag_products.TabIndex = 2;
            // 
            // c_nombre
            // 
            c_nombre.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_nombre.HeaderText = "Nombre";
            c_nombre.Name = "c_nombre";
            c_nombre.ReadOnly = true;
            c_nombre.Resizable = DataGridViewTriState.False;
            // 
            // c_precio
            // 
            c_precio.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_precio.HeaderText = "Precio";
            c_precio.Name = "c_precio";
            c_precio.ReadOnly = true;
            // 
            // c_ings
            // 
            c_ings.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_ings.HeaderText = "Ingredientes";
            c_ings.Name = "c_ings";
            c_ings.ReadOnly = true;
            // 
            // btn_buscar
            // 
            btn_buscar.Location = new Point(282, 49);
            btn_buscar.Name = "btn_buscar";
            btn_buscar.Size = new Size(75, 23);
            btn_buscar.TabIndex = 3;
            btn_buscar.Text = "Buscar";
            btn_buscar.UseVisualStyleBackColor = true;
            btn_buscar.Click += btn_buscar_Click;
            // 
            // chk_promo
            // 
            chk_promo.AutoSize = true;
            chk_promo.Location = new Point(131, 73);
            chk_promo.Name = "chk_promo";
            chk_promo.Size = new Size(85, 19);
            chk_promo.TabIndex = 4;
            chk_promo.Text = "Promocion";
            chk_promo.UseVisualStyleBackColor = true;
            // 
            // btn_select
            // 
            btn_select.Location = new Point(193, 467);
            btn_select.Name = "btn_select";
            btn_select.Size = new Size(104, 23);
            btn_select.TabIndex = 5;
            btn_select.Text = "Seleccionar";
            btn_select.UseVisualStyleBackColor = true;
            btn_select.Click += btn_select_Click;
            // 
            // txt_cantidad
            // 
            txt_cantidad.Location = new Point(193, 398);
            txt_cantidad.Name = "txt_cantidad";
            txt_cantidad.Size = new Size(145, 23);
            txt_cantidad.TabIndex = 7;
            txt_cantidad.KeyPress += txt_cantidad_KeyPress;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(86, 401);
            label2.Name = "label2";
            label2.Size = new Size(94, 15);
            label2.TabIndex = 6;
            label2.Text = "Ingrese cantidad";
            // 
            // lb_cantidad
            // 
            lb_cantidad.AutoSize = true;
            lb_cantidad.ForeColor = Color.Red;
            lb_cantidad.Location = new Point(193, 434);
            lb_cantidad.Name = "lb_cantidad";
            lb_cantidad.Size = new Size(0, 15);
            lb_cantidad.TabIndex = 8;
            // 
            // ViewProducts
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(548, 548);
            Controls.Add(lb_cantidad);
            Controls.Add(txt_cantidad);
            Controls.Add(label2);
            Controls.Add(btn_select);
            Controls.Add(chk_promo);
            Controls.Add(btn_buscar);
            Controls.Add(datag_products);
            Controls.Add(txt_name);
            Controls.Add(label1);
            Name = "ViewProducts";
            Text = "ViewProducts";
            ((System.ComponentModel.ISupportInitialize)datag_products).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private TextBox txt_name;
        private DataGridView datag_products;
        private Button btn_buscar;
        private CheckBox chk_promo;
        private Button btn_select;
        private DataGridViewTextBoxColumn c_nombre;
        private DataGridViewTextBoxColumn c_precio;
        private DataGridViewTextBoxColumn c_ings;
        private TextBox txt_cantidad;
        private Label label2;
        private Label lb_cantidad;
    }
}