﻿using Chesseland.db; // Importa el namespace Chesseland.db

namespace Chesseland.forms
{
    /// <summary>
    /// Clase DelUser
    /// </summary>
    public partial class DelUser : Form
    {
        internal static User? tempuser; // Variable estática para almacenar temporalmente un usuario
        /// <summary>
        /// Constructor de la clase DelUser
        /// </summary>
        public DelUser()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se carga el formulario
        private void DelUser_Load(object sender, EventArgs e)
        {
            // Este método podría contener lógica adicional al cargar el formulario, pero está vacío en este caso.
        }

        // Evento que se dispara cuando se hace clic en el botón de buscar
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            datagridusers.Rows.Clear(); // Limpia las filas del DataGridView
            if (!string.IsNullOrEmpty(btn_buscar.Text)) // Verifica si el texto del botón de buscar no está vacío
            {
                Buscar(txt_nombre.Text); // Llama al método Buscar con el nombre introducido en el campo de texto
            }
        }

        // Evento que se dispara cuando se hace clic en el botón de modificar
        private void btn_mod_Click(object sender, EventArgs e)
        {
            if (datagridusers.Rows.Count != 0) // Verifica si hay filas en el DataGridView
            {
                try
                {
                    // Crea una nueva instancia de User con los datos obtenidos del DataGridView
                    tempuser = new()
                    {
                        IdUser = (int)GetDataValues(0),
                        Username = (string)GetDataValues(1),
                        Rol = (Roles)GetDataValues(2),
                        Email = (string)GetDataValues(3),
                    };

                    var mod = new ModUser(); // Crea una instancia del formulario ModUser
                    mod.ShowDialog(); // Muestra el formulario ModUser como cuadro de diálogo modal
                    Buscar("*"); // Llama al método Buscar para actualizar la lista de usuarios

                }
                catch (Exception ex) { MessageBox.Show($"Error: {ex.Message}"); } // Captura y muestra cualquier excepción que ocurra
            }
        }

        // Evento que se dispara cuando se hace clic en el botón de eliminar
        private void btn_elim_Click(object sender, EventArgs e)
        {
            if (datagridusers.Rows.Count != 0) // Verifica si hay filas en el DataGridView
            {
                try
                {
                    var id = (int)GetDataValues(0); // Obtiene el ID del usuario seleccionado
                    var name = (string)GetDataValues(1); // Obtiene el nombre del usuario seleccionado
                    var rol = (Roles)GetDataValues(2); // Obtiene el rol del usuario seleccionado
                    var email = (string)GetDataValues(3); // Obtiene el correo electrónico del usuario seleccionado
                    if (rol != Roles.Admin) // Verifica si el rol del usuario no es Administrador
                    {
                        // Muestra un cuadro de diálogo para confirmar la eliminación del usuario
                        if (MessageBox.Show($"Esta seguro que desea borrar al usuario {name}?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            var user = new User(); // Crea una nueva instancia de User
                            user.IdUser = id; // Asigna el ID del usuario a eliminar
                            user.Username = name; // Asigna el nombre del usuario a eliminar
                            user.Rol = rol; // Asigna el rol del usuario a eliminar
                            user.Email = email; // Asigna el correo electrónico del usuario a eliminar
                            Helpers.users!.RemoveUser(user); // Elimina al usuario utilizando el helper de usuarios
                            Buscar("*"); // Llama al método Buscar para actualizar la lista de usuarios
                            MessageBox.Show("Usuario eliminado correctamente"); // Muestra un mensaje de éxito
                        }
                    }
                    else
                    {
                        MessageBox.Show("No puedes borrar a un administrador.", "¡Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information); // Muestra un mensaje indicando que no se puede borrar a un administrador
                    }
                }
                catch (Exception ex) { MessageBox.Show($"Error: {ex.Message}"); } // Captura y muestra cualquier excepción que ocurra
            }
        }

        // Método para obtener los valores de la celda en el DataGridView
        private object GetDataValues(int index)
        {
            DataGridViewCell a = datagridusers.CurrentCell; // Obtiene la celda actualmente seleccionada en el DataGridView
            return datagridusers.Rows[a.RowIndex].Cells[index].Value; // Devuelve el valor de la celda en la fila actual y el índice especificado
        }

        // Método para buscar usuarios según el nombre especificado
        private void Buscar(string nombre)
        {
            if (nombre == "*" || nombre == string.Empty) // Verifica si se desea buscar todos los usuarios o si el nombre está vacío
            {
                var u = Helpers.users!.GetUsers(); // Obtiene todos los usuarios utilizando el helper de usuarios
                datagridusers.Rows.Clear(); // Limpia las filas del DataGridView
                foreach (var user in u) // Recorre cada usuario encontrado
                {
                    int index = datagridusers.Rows.Add(); // Añade una nueva fila al DataGridView
                    datagridusers.Rows[index].Cells[0].Value = user.IdUser; // Asigna el ID del usuario a la celda
                    datagridusers.Rows[index].Cells[1].Value = user.Username; // Asigna el nombre del usuario a la celda
                    datagridusers.Rows[index].Cells[2].Value = user.Rol; // Asigna el rol del usuario a la celda
                    datagridusers.Rows[index].Cells[3].Value = user.Email; // Asigna el correo electrónico del usuario a la celda
                }
            }
            else
            {
                var user = Helpers.users?.GetUserByName(txt_nombre.Text); // Busca un usuario por nombre utilizando el helper de usuarios
                if (user != null) // Verifica si se encontró un usuario
                {
                    int index = datagridusers.Rows.Add(); // Añade una nueva fila al DataGridView
                    datagridusers.Rows[index].Cells[0].Value = user.IdUser; // Asigna el ID del usuario a la celda
                    datagridusers.Rows[index].Cells[1].Value = user.Username; // Asigna el nombre del usuario a la celda
                    datagridusers.Rows[index].Cells[2].Value = user.Rol; // Asigna el rol del usuario a la celda
                    datagridusers.Rows[index].Cells[3].Value = user.Email; // Asigna el correo electrónico del usuario a la celda
                }
            }
        }
    }
}
