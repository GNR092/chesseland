﻿using Chesseland.db; // Importa el namespace Chesseland.db
using MySql.Data.MySqlClient; // Importa la clase MySqlConnection del conector MySQL
using Mysqlext.Extensions; // Importa las extensiones de MySQL
using System.Data; // Importa el namespace System.Data para trabajar con tipos de datos de ADO.NET

namespace Chesseland.init
{
    /// <summary>
    /// Formulario para configurar la conexión inicial a la base de datos y crear un usuario administrador.
    /// </summary>
    public partial class Registro : Form
    {
        private IDbConnection? _connection; // Variable para almacenar la conexión a la base de datos
        private Config? cnf; // Configuración de conexión

        /// <summary>
        /// 
        /// </summary>
        public Registro()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        /// <summary>
        /// Maneja el evento de clic en el botón de prueba de conexión.
        /// Prueba la conexión a la base de datos utilizando los datos ingresados.
        /// </summary>
        private void btn_test_Click(object sender, EventArgs e)
        {
            // Crea una nueva configuración de conexión con los datos ingresados en los campos del formulario
            cnf = new Config()
            {
                Database = txt_database.Text,
                Username = txt_user.Text,
                Password = txt_pass.Text,
                Host = txt_host.Text,
                Port = Convert.ToInt32(txt_port.Text)
            };

            _connection = new MySqlConnection(); // Inicializa una nueva instancia de MySqlConnection para la conexión

            // Establece la cadena de conexión dependiendo del estado (0 o 1)
            _connection.ConnectionString = ctConnectionString(0);

            // Verifica si la conexión no es nula y procede a realizar la conexión
            if (_connection != null)
            {
                try
                {
                    // Utiliza una conexión temporal para abrir la conexión y verificar su éxito
                    using var tmpdb = _connection.CloneEx();
                    tmpdb.Open();
                    MessageBox.Show("Conexión exitosa."); // Muestra un mensaje de conexión exitosa
                    btn_save.Enabled = true; // Habilita el botón de guardar configuración
                }
                catch
                {
                    MessageBox.Show("Conexión fallida."); // Muestra un mensaje de conexión fallida si hay un error
                }
            }
        }

        /// <summary>
        /// Maneja el evento de clic en el botón de guardar.
        /// Guarda la configuración y crea un usuario administrador en la base de datos.
        /// </summary>
        private void btn_save_Click(object sender, EventArgs e)
        {
            // Verifica el texto del botón de acuerdo a su estado
            if (btn_save.Text == "Siguiente")
            {
                // Asigna la configuración actual a Helpers.Cnf y configura los elementos visuales del formulario para el siguiente paso
                Helpers.Cnf = cnf;
                txt_database.Hide();
                pc_base.Hide();
                lbl_base.Text = "Ingresar los datos para el usuario administrador.";
                lbl_user.Text = "Usuario admin:";
                toolTip1.SetToolTip(pc_usuario, "Usuario administrador a crear para inicio de sessión.");
                txt_user.Clear();
                txt_pass.Clear();
                toolTip1.SetToolTip(pc_contra, "Contraseña para el usuario administrador.");
                lbl_host.Text = "Re-Contraseña:";
                toolTip1.SetToolTip(pc_host, "Repita la contraseña anterior.");
                txt_host.Clear();
                txt_host.UseSystemPasswordChar = true;
                lbl_port.Hide();
                txt_port.Hide();
                pc_port.Hide();
                btn_test.Hide();
                btn_save.Text = "Guardar"; // Cambia el texto del botón a "Guardar"
            }
            else if (btn_save.Text == "Guardar")
            {
                // Verifica si las contraseñas coinciden antes de continuar
                if (txt_pass.Text != txt_host.Text)
                {
                    MessageBox.Show("las contraseñas no coinciden.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return; // Sale del método si las contraseñas no coinciden
                }
                else
                {
                    try
                    {
                        var user = new User(); // Crea una nueva instancia de User
                        user.Username = txt_user.Text; // Asigna el nombre de usuario ingresado
                        user.Rol = Roles.Superadmin; // Asigna el rol de superadmin al usuario
                        user.CreateBCryptHash(txt_pass.Text); // Crea un hash bcrypt para la contraseña ingresada
                        user.Email = ""; // Deja el email del usuario vacío

                        Helpers.Dbmanager = new(_connection!); // Inicializa Helpers.Dbmanager con la conexión establecida

                        // Verifica si la base de datos especificada ya existe
                        if (Helpers.Dbmanager.ExistDataBase(cnf!.Database!))
                        {
                            _connection!.ConnectionString = ctConnectionString(1); // Establece la cadena de conexión para incluir la base de datos
                            Helpers.users = new(_connection!); // Inicializa Helpers.users con la conexión establecida
                            Helpers.users.AddUser(user); // Agrega el usuario a la base de datos
                        }
                        else
                        {
                            Helpers.Dbmanager.CreateDataBase(cnf!.Database!); // Crea la base de datos especificada
                            _connection!.ConnectionString = ctConnectionString(1); // Establece la cadena de conexión para incluir la base de datos
                            Helpers.users = new(_connection!); // Inicializa Helpers.users con la conexión establecida
                            Helpers.users.AddUser(user); // Agrega el usuario a la base de datos
                        }

                        cnf!.EncriptConnectionData(); // Encripta los datos de conexión en la configuración
                        Helpers.DB = _connection!; // Asigna la conexión a Helpers.DB
                        Helpers.products = new ProductManager(Helpers.DB!); // Inicializa Helpers.products con la conexión establecida
                        Helpers.promos = new PromoManager(Helpers.DB!); // Inicializa Helpers.promos con la conexión establecida
                        Helpers.code = 1; // Asigna el código 1 a Helpers.code
                        MessageBox.Show("Guardado con exito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information); // Muestra un mensaje de éxito
                        Close(); // Cierra el formulario
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                }
            }
        }

        /// <summary>
        /// Crea la cadena de conexión a la base de datos dependiendo del estado.
        /// </summary>
        private string ctConnectionString(int state)
        {
            // Verifica el estado para determinar qué tipo de cadena de conexión crear
            if (state == 0)
            {
                // Retorna una cadena de conexión sin incluir la base de datos
                return string.Format("server={0};user={1};port={2};password={3};",
                cnf!.Host,
                cnf.Username,
                cnf.Port,
                cnf.Password
                );
            }
            else
            {
                // Retorna una cadena de conexión incluyendo la base de datos
                return string.Format("server={0};user={1};database={2};port={3};password={4};",
                cnf!.Host,
                cnf.Username,
                cnf.Database,
                cnf.Port,
                cnf.Password
                );
            }
        }
    }
}
