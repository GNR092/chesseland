﻿namespace Chesseland.init
{
    partial class Registro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            label1 = new Label();
            label2 = new Label();
            groupBox1 = new GroupBox();
            pc_port = new PictureBox();
            pc_host = new PictureBox();
            pc_contra = new PictureBox();
            pc_usuario = new PictureBox();
            pc_base = new PictureBox();
            btn_save = new Button();
            btn_test = new Button();
            txt_port = new TextBox();
            lbl_port = new Label();
            txt_host = new TextBox();
            lbl_host = new Label();
            txt_pass = new TextBox();
            lbl_pass = new Label();
            txt_user = new TextBox();
            lbl_user = new Label();
            txt_database = new TextBox();
            lbl_base = new Label();
            toolTip1 = new ToolTip(components);
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pc_port).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pc_host).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pc_contra).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pc_usuario).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pc_base).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(10, 17);
            label1.Name = "label1";
            label1.Size = new Size(397, 19);
            label1.TabIndex = 0;
            label1.Text = "Bienvenido al inicio de la configuracion de ChesseLand";
            label1.TextAlign = ContentAlignment.TopCenter;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(41, 46);
            label2.Name = "label2";
            label2.Size = new Size(351, 19);
            label2.TabIndex = 1;
            label2.Text = "porfavor ingrese los datos  datos de la conexión.";
            label2.TextAlign = ContentAlignment.TopCenter;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(pc_port);
            groupBox1.Controls.Add(pc_host);
            groupBox1.Controls.Add(pc_contra);
            groupBox1.Controls.Add(pc_usuario);
            groupBox1.Controls.Add(pc_base);
            groupBox1.Controls.Add(btn_save);
            groupBox1.Controls.Add(btn_test);
            groupBox1.Controls.Add(txt_port);
            groupBox1.Controls.Add(lbl_port);
            groupBox1.Controls.Add(txt_host);
            groupBox1.Controls.Add(lbl_host);
            groupBox1.Controls.Add(txt_pass);
            groupBox1.Controls.Add(lbl_pass);
            groupBox1.Controls.Add(txt_user);
            groupBox1.Controls.Add(lbl_user);
            groupBox1.Controls.Add(txt_database);
            groupBox1.Controls.Add(lbl_base);
            groupBox1.Location = new Point(18, 76);
            groupBox1.Margin = new Padding(3, 2, 3, 2);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new Padding(3, 2, 3, 2);
            groupBox1.Size = new Size(416, 290);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            // 
            // pc_port
            // 
            pc_port.Image = Properties.Resources.lmsgCntr0;
            pc_port.Location = new Point(282, 188);
            pc_port.Name = "pc_port";
            pc_port.Size = new Size(19, 19);
            pc_port.SizeMode = PictureBoxSizeMode.StretchImage;
            pc_port.TabIndex = 16;
            pc_port.TabStop = false;
            toolTip1.SetToolTip(pc_port, "Puerto de la base de datos normalmente 3306");
            // 
            // pc_host
            // 
            pc_host.Image = Properties.Resources.lmsgCntr0;
            pc_host.Location = new Point(282, 144);
            pc_host.Name = "pc_host";
            pc_host.Size = new Size(19, 19);
            pc_host.SizeMode = PictureBoxSizeMode.StretchImage;
            pc_host.TabIndex = 15;
            pc_host.TabStop = false;
            toolTip1.SetToolTip(pc_host, "Dirección ip de la base de datos en caso de que no se encuentre en local");
            // 
            // pc_contra
            // 
            pc_contra.Image = Properties.Resources.lmsgCntr0;
            pc_contra.Location = new Point(282, 102);
            pc_contra.Name = "pc_contra";
            pc_contra.Size = new Size(19, 19);
            pc_contra.SizeMode = PictureBoxSizeMode.StretchImage;
            pc_contra.TabIndex = 14;
            pc_contra.TabStop = false;
            toolTip1.SetToolTip(pc_contra, "Contraseña de la base de datos normalmente de root");
            // 
            // pc_usuario
            // 
            pc_usuario.Image = Properties.Resources.lmsgCntr0;
            pc_usuario.Location = new Point(282, 63);
            pc_usuario.Name = "pc_usuario";
            pc_usuario.Size = new Size(19, 19);
            pc_usuario.SizeMode = PictureBoxSizeMode.StretchImage;
            pc_usuario.TabIndex = 13;
            pc_usuario.TabStop = false;
            toolTip1.SetToolTip(pc_usuario, "Usuario de la base de datos comunmente como root");
            // 
            // pc_base
            // 
            pc_base.Image = Properties.Resources.lmsgCntr0;
            pc_base.Location = new Point(282, 28);
            pc_base.Name = "pc_base";
            pc_base.Size = new Size(19, 19);
            pc_base.SizeMode = PictureBoxSizeMode.StretchImage;
            pc_base.TabIndex = 12;
            pc_base.TabStop = false;
            toolTip1.SetToolTip(pc_base, "Nombre de la base de datos a crear si no existe");
            // 
            // btn_save
            // 
            btn_save.Enabled = false;
            btn_save.Location = new Point(169, 229);
            btn_save.Margin = new Padding(3, 2, 3, 2);
            btn_save.Name = "btn_save";
            btn_save.Size = new Size(82, 22);
            btn_save.TabIndex = 11;
            btn_save.Text = "Siguiente";
            btn_save.UseVisualStyleBackColor = true;
            btn_save.Click += btn_save_Click;
            // 
            // btn_test
            // 
            btn_test.Location = new Point(81, 229);
            btn_test.Margin = new Padding(3, 2, 3, 2);
            btn_test.Name = "btn_test";
            btn_test.Size = new Size(82, 22);
            btn_test.TabIndex = 10;
            btn_test.Text = "Probar";
            btn_test.UseVisualStyleBackColor = true;
            btn_test.Click += btn_test_Click;
            // 
            // txt_port
            // 
            txt_port.Location = new Point(166, 184);
            txt_port.Margin = new Padding(3, 2, 3, 2);
            txt_port.Name = "txt_port";
            txt_port.Size = new Size(110, 23);
            txt_port.TabIndex = 9;
            txt_port.Text = "3306";
            // 
            // lbl_port
            // 
            lbl_port.AutoSize = true;
            lbl_port.Location = new Point(69, 184);
            lbl_port.Name = "lbl_port";
            lbl_port.Size = new Size(45, 15);
            lbl_port.TabIndex = 8;
            lbl_port.Text = "Puerto;";
            // 
            // txt_host
            // 
            txt_host.Location = new Point(166, 140);
            txt_host.Margin = new Padding(3, 2, 3, 2);
            txt_host.Name = "txt_host";
            txt_host.Size = new Size(110, 23);
            txt_host.TabIndex = 7;
            txt_host.Text = "localhost";
            // 
            // lbl_host
            // 
            lbl_host.AutoSize = true;
            lbl_host.Location = new Point(69, 140);
            lbl_host.Name = "lbl_host";
            lbl_host.Size = new Size(35, 15);
            lbl_host.TabIndex = 6;
            lbl_host.Text = "Host:";
            // 
            // txt_pass
            // 
            txt_pass.Location = new Point(166, 98);
            txt_pass.Margin = new Padding(3, 2, 3, 2);
            txt_pass.Name = "txt_pass";
            txt_pass.Size = new Size(110, 23);
            txt_pass.TabIndex = 5;
            txt_pass.Text = "123456";
            txt_pass.UseSystemPasswordChar = true;
            // 
            // lbl_pass
            // 
            lbl_pass.AutoSize = true;
            lbl_pass.Location = new Point(69, 98);
            lbl_pass.Name = "lbl_pass";
            lbl_pass.Size = new Size(70, 15);
            lbl_pass.TabIndex = 4;
            lbl_pass.Text = "Contraseña:";
            // 
            // txt_user
            // 
            txt_user.Location = new Point(166, 59);
            txt_user.Margin = new Padding(3, 2, 3, 2);
            txt_user.Name = "txt_user";
            txt_user.Size = new Size(110, 23);
            txt_user.TabIndex = 3;
            txt_user.Text = "alumno";
            // 
            // lbl_user
            // 
            lbl_user.AutoSize = true;
            lbl_user.Location = new Point(69, 59);
            lbl_user.Name = "lbl_user";
            lbl_user.Size = new Size(50, 15);
            lbl_user.TabIndex = 2;
            lbl_user.Text = "Usuario:";
            // 
            // txt_database
            // 
            txt_database.Location = new Point(166, 25);
            txt_database.Margin = new Padding(3, 2, 3, 2);
            txt_database.Name = "txt_database";
            txt_database.Size = new Size(110, 23);
            txt_database.TabIndex = 1;
            txt_database.Text = "chesseland";
            // 
            // lbl_base
            // 
            lbl_base.AutoSize = true;
            lbl_base.Location = new Point(69, 28);
            lbl_base.Name = "lbl_base";
            lbl_base.Size = new Size(82, 15);
            lbl_base.TabIndex = 0;
            lbl_base.Text = "Base de datos:";
            // 
            // Registro
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(457, 390);
            Controls.Add(groupBox1);
            Controls.Add(label2);
            Controls.Add(label1);
            Margin = new Padding(3, 2, 3, 2);
            Name = "Registro";
            Text = "registro";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pc_port).EndInit();
            ((System.ComponentModel.ISupportInitialize)pc_host).EndInit();
            ((System.ComponentModel.ISupportInitialize)pc_contra).EndInit();
            ((System.ComponentModel.ISupportInitialize)pc_usuario).EndInit();
            ((System.ComponentModel.ISupportInitialize)pc_base).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private GroupBox groupBox1;
        private TextBox txt_user;
        private Label lbl_user;
        private TextBox txt_database;
        private Label lbl_base;
        private TextBox txt_pass;
        private Label lbl_pass;
        private TextBox txt_port;
        private Label lbl_port;
        private TextBox txt_host;
        private Label lbl_host;
        private Button btn_save;
        private Button btn_test;
        private PictureBox pc_base;
        private PictureBox pc_port;
        private PictureBox pc_host;
        private PictureBox pc_contra;
        private PictureBox pc_usuario;
        private ToolTip toolTip1;
    }
}