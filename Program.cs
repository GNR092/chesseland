namespace Chesseland
{
    internal static class Program
    {
        /// <summary>
        /// Punto de entrada principal de la aplicaci�n.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Habilita estilos visuales de la aplicaci�n
            Application.EnableVisualStyles();

            // Establece el renderizado de texto compatible
            Application.SetCompatibleTextRenderingDefault(false);

            // Configura el modo de alta DPI seg�n el sistema operativo
            Application.SetHighDpiMode(HighDpiMode.SystemAware);

            // Inicializa la configuraci�n de la aplicaci�n
            ApplicationConfiguration.Initialize();

            // Ejecuta el formulario de inicio de sesi�n al iniciar la aplicaci�n
            Application.Run(new login.Login());
        }
    }
}
