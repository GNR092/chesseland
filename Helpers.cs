﻿using Chesseland.db; // Importa el namespace Chesseland.db para acceder a la capa de datos
using Chesseland; // Importa el namespace Chesseland para acceder a otras funcionalidades del sistema
using System.Data; // Importa System.Data para trabajar con tipos de datos de bases de datos
using Mysqlext; // Importa Mysqlext para acceder a las extensiones específicas de MySQL
using System.ComponentModel; // Importa el namespace System.ComponentModel para trabajar con componentes

/// <summary>
/// Clase auxiliar que contiene variables y métodos útiles para el funcionamiento del sistema.
/// </summary>
internal static class Helpers
{
    /// <summary>
    /// Representa la conexión a la base de datos.
    /// </summary>
    public static IDbConnection? DB;

    /// <summary>
    /// Representa la configuración de conexión a la base de datos.
    /// </summary>
    public static Config? Cnf;

    /// <summary>
    /// Representa el administrador de la base de datos.
    /// </summary>
    public static DatabaseManager? Dbmanager;

    /// <summary>
    /// Representa el administrador de usuarios.
    /// </summary>
    public static UserManager? users;

    /// <summary>
    /// Representa el administrador de productos.
    /// </summary>
    public static ProductManager? products;

    /// <summary>
    /// Representa el administrador de promociones.
    /// </summary>
    public static PromoManager? promos;

    /// <summary>
    /// Representa el administrador de ventas.
    /// </summary>
#pragma warning disable CS0649 // El campo 'Helpers.sales' nunca se asigna y siempre tendrá el valor predeterminado null
    public static SaleManager? sales;
#pragma warning restore CS0649 // El campo 'Helpers.sales' nunca se asigna y siempre tendrá el valor predeterminado null

    /// <summary>
    /// Lista temporal de ingredientes.
    /// </summary>
    public static List<string>? TempIngds;

    /// <summary>
    /// Promoción temporal actual.
    /// </summary>
    public static Promo? temppromo = null;

    /// <summary>
    /// Indicador de inicio de sesión.
    /// </summary>
    public static bool login;

    /// <summary>
    /// BackgroundWorker para realizar tareas en segundo plano.
    /// </summary>
    public static BackgroundWorker? worker;

    /// <summary>
    /// Usuario actualmente logueado.
    /// </summary>
    public static User? CurrentUser { get; set; }

    /// <summary>
    /// Código de estado.
    /// </summary>
    public static int code = 0;

    // Constantes para validación de caracteres
    private const string abc = "abcdefghijklmnñopqrstuvwxyzáéíóúü \b";
    private const string nums = "1234567890\b";

    #region Extensiones
    /// <summary>
    /// Método de extensión: obtiene el rol a partir de un string.
    /// </summary>
    /// <param name="rolString">String que representa el rol</param>
    /// <returns>Rol en formato enum de Roles</returns>
    public static Roles GetRol(this string rolString)
    {
        switch (rolString.ToLower())
        {
            case "admin":
                return Roles.Admin;
            case "cajero":
                return Roles.Cajero;
            case "mesero":
                return Roles.Mesero;
            default:
                return Roles.None;
        }
    }

    /// <summary>
    /// Método de extensión: convierte un Rol en formato string.
    /// </summary>
    /// <param name="rol">Rol en formato enum</param>
    /// <returns>Rol en formato string</returns>
    public static string GetRolToString(this Roles rol)
    {
        switch (rol)
        {
            case Roles.Admin:
                return "Admin";
            case Roles.Cajero:
                return "Cajero";
            case Roles.Mesero:
                return "Mesero";
            default:
                return "None";
        }
    }

    /// <summary>
    /// Método de extensión: comprueba si un carácter es una letra.
    /// </summary>
    /// <param name="s">Carácter a comprobar</param>
    /// <returns>true si es una letra, false si no lo es</returns>
    public static bool IsOnlyLetter(this char s)
    {
        var c = s.ToString().ToLower();
        if (!abc.Contains(c)) return true; // Retorna true si no es una letra
        return false; // Retorna false si es una letra
    }

    /// <summary>
    /// Método de extensión: comprueba si un carácter es un número.
    /// </summary>
    /// <param name="s">Carácter a comprobar</param>
    /// <returns>true si es un número, false si no lo es</returns>
    public static bool IsOnlyNums(this char s)
    {
        var c = s.ToString().ToLower();
        if (!nums.Contains(c)) return true; // Retorna true si no es un número
        return false; // Retorna false si es un número
    }

    /// <summary>
    /// Método de extensión: comprueba si la tecla presionada es Enter.
    /// </summary>
    /// <param name="key">Tecla presionada</param>
    /// <returns>true si la tecla presionada es Enter, false si no lo es</returns>
    public static bool IsKeyEnterPress(this char key)
    {
        if ((Keys)key == Keys.Enter) return true; // Retorna true si la tecla presionada es Enter
        return false; // Retorna false si la tecla presionada no es Enter
    }

    /// <summary>
    /// Método de extensión: verifica si un TextBox es nulo o está vacío.
    /// </summary>
    /// <param name="textBox">TextBox a verificar</param>
    /// <returns>true si el TextBox es nulo o está vacío, false si no lo es</returns>
    public static bool IsNullOrEmpty(this TextBox textBox)
    {
        if (textBox == null) return true; // Retorna true si el TextBox es nulo
        else if (string.IsNullOrEmpty(textBox.Text)) return true; // Retorna true si el texto dentro del TextBox está vacío
        return false; // Retorna false si el TextBox no es nulo y tiene texto
    }
    #endregion
}
