﻿using Chesseland.db; // Importa el namespace Chesseland.db
using Chesseland.formsaux; // Importa el namespace Chesseland.formsaux

namespace Chesseland
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AddProducts : Form
    {
        private bool _move = false; // Variable para controlar el movimiento del formulario
        private int mx, my; // Variables para almacenar las coordenadas del ratón

        /// <summary>
        /// 
        /// </summary>
        public AddProducts()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se presiona el botón del ratón en el formulario
        private void AddProducts_MouseDown(object sender, MouseEventArgs e)
        {
            _move = true; // Activa el movimiento del formulario
            mx = e.X; my = e.Y; // Almacena las coordenadas del ratón
        }

        // Evento que se dispara cuando se mueve el ratón sobre el formulario
        private void AddProducts_MouseMove(object sender, MouseEventArgs e)
        {
            if (_move) SetDesktopLocation(MousePosition.X - mx, MousePosition.Y - my); // Mueve el formulario con el ratón
        }

        // Evento que se dispara cuando se suelta el botón del ratón en el formulario
        private void AddProducts_MouseUp(object sender, MouseEventArgs e)
        {
            _move = false; // Desactiva el movimiento del formulario
        }

        // Evento que se dispara cuando se hace clic en el label6 (minimizar ventana)
        private void label6_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized; // Minimiza la ventana
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de nombre del producto
        private void txt_product_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar.IsOnlyLetter(); // Verifica si el carácter ingresado es una letra
            if (e.Handled) lb_product.Text = "Solo permiten letras."; // Muestra un mensaje si el carácter no es una letra
            else lb_product.Text = ""; // Limpia el mensaje si el carácter es válido
        }

        // Evento que se dispara cuando se hace clic en el botón de salir
        private void btn_exit_Click(object sender, EventArgs e)
        {
            Helpers.TempIngds = null; // Resetea los ingredientes temporales
            Close(); // Cierra el formulario
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de precio
        private void txt_precio_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar.IsOnlyNums(); // Verifica si el carácter ingresado es un número
            if (e.Handled) lb_precio.Text = "Solo se permiten numeros."; // Muestra un mensaje si el carácter no es un número
            else lb_precio.Text = ""; // Limpia el mensaje si el carácter es válido
        }

        // Evento que se dispara cuando se hace clic en el botón de buscar
        private void btn_search_Click(object sender, EventArgs e)
        {
            var p = new Producto(); // Crea una nueva instancia de Producto
           
        }

        // Evento que se dispara cuando se hace clic en el botón de agregar producto
        private void btn_add_Click(object sender, EventArgs e)
        {
            var p = new Producto(); // Crea una nueva instancia de Producto
            if (!txt_product.IsNullOrEmpty() && !txt_precio.IsNullOrEmpty() && Helpers.TempIngds != null) // Verifica si los campos no están vacíos y hay ingredientes temporales
            {
                if (Helpers.TempIngds!.Count != 0) // Verifica si hay al menos un ingrediente
                {
                    p.Nombre = txt_product.Text; // Asigna el nombre del producto
                    p.Precio = int.Parse(txt_precio.Text); // Asigna el precio del producto
                    p.Ingredientes = string.Join(',', Helpers.TempIngds!); // Asigna los ingredientes del producto
                    try
                    {
                        Helpers.products!.AddProduct(p); // Añade el producto a la lista de productos
                        Clear(); // Limpia los campos del formulario
                        MessageBox.Show("Producto agregado con exito."); // Muestra un mensaje de éxito
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message, "Ha ocurrido un error"); } // Muestra un mensaje de error en caso de excepción
                }
                else
                    MessageBox.Show("Debe seleccional al menos 1 ingrediente"); // Muestra un mensaje si no hay ingredientes
            }
            else
                MessageBox.Show("Verifique los campos."); // Muestra un mensaje si los campos están vacíos
        }

        // Evento que se dispara cuando se hace clic en el botón de seleccionar ingrediente
        private void btn_sing_Click(object sender, EventArgs e)
        {
            var ing = new AddIngredient(); // Crea una nueva instancia del formulario AddIngredient
            ing.ShowDialog(); // Muestra el formulario de agregar ingrediente como un cuadro de diálogo
            dataGridIngredientes.Rows.Clear(); // Limpia las filas del DataGridView
            if (Helpers.TempIngds != null) // Verifica si hay ingredientes temporales
            {
                // Añade los ingredientes temporales al DataGridView
                foreach (var i in Helpers.TempIngds!)
                {
                    int index = dataGridIngredientes.Rows.Add(); // Agrega una nueva fila al DataGridView
                    dataGridIngredientes.Rows[index].Cells[0].Value = i; // Asigna el ingrediente a la celda
                }
            }
        }

        // Evento que se dispara cuando se hace clic en el botón de eliminar ingrediente
        private void btn_deling_Click(object sender, EventArgs e)
        {
            if (dataGridIngredientes.Rows.Count != 0) // Verifica si hay filas en el DataGridView
            {
                if (dataGridIngredientes.SelectedRows.Count != 0) // Verifica si hay filas seleccionadas
                {
                    if (dataGridIngredientes.Rows.Count == dataGridIngredientes.SelectedRows.Count) // Verifica si todas las filas están seleccionadas
                        dataGridIngredientes.Rows.Clear(); // Limpia todas las filas del DataGridView
                    // Elimina las filas seleccionadas
                    foreach (DataGridViewRow dataGridViewRow in (BaseCollection)dataGridIngredientes.SelectedRows)
                    {
                        try
                        {
                            dataGridIngredientes.Rows.Remove(dataGridViewRow); // Elimina la fila del DataGridView
                            var ingred = (string)dataGridViewRow.Cells[0].Value; // Obtiene el valor de la celda
                            Helpers.TempIngds!.Remove(ingred); // Elimina el ingrediente de la lista temporal
                        }
                        catch { } // Captura cualquier excepción que pueda ocurrir
                    }
                }
                else
                {
                    MessageBox.Show("No ha seleccionado una fila a eliminar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand); // Muestra un mensaje si no se seleccionó ninguna fila
                }
            }
            else
            {
                MessageBox.Show("No hay ninguna fila a borrar!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand); // Muestra un mensaje si no hay filas en el DataGridView
            }
        }

        // Método para limpiar los campos del formulario
        private void Clear()
        {
            foreach (Control control in Controls) // Recorre todos los controles del formulario
            {
                if (control is TextBox textBox) // Si el control es un TextBox
                {
                    textBox.Clear(); // Limpia el texto del TextBox
                }
            }
            dataGridIngredientes.Rows.Clear(); // Limpia las filas del DataGridView
        }
    }
}
