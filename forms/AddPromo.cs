﻿using Chesseland.db; // Importa el namespace Chesseland.db
using Chesseland.forms; // Importa el namespace Chesseland.forms
using Chesseland.formsaux; // Importa el namespace Chesseland.formsaux

namespace Chesseland
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AddPromo : Form
    {
        private bool _move = false; // Variable para controlar el movimiento del formulario
        private int mx, my; // Variables para almacenar las coordenadas del ratón
        private Promo? currentpromo; // Variable para almacenar la promoción actual

        /// <summary>
        /// 
        /// </summary>
        public AddPromo()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de inicio
        private void btn_inicio_Click(object sender, EventArgs e)
        {
            Helpers.TempIngds = null; // Resetea los ingredientes temporales
            Close(); // Cierra el formulario
        }

        // Evento que se dispara cuando se presiona el botón del ratón en el formulario
        private void AddPromo_MouseDown(object sender, MouseEventArgs e)
        {
            _move = true; // Activa el movimiento del formulario
            mx = e.X; my = e.Y; // Almacena las coordenadas del ratón
        }

        // Evento que se dispara cuando se mueve el ratón sobre el formulario
        private void AddPromo_MouseMove(object sender, MouseEventArgs e)
        {
            if (_move) SetDesktopLocation(MousePosition.X - mx, MousePosition.Y - my); // Mueve el formulario con el ratón
        }

        // Evento que se dispara cuando se suelta el botón del ratón en el formulario
        private void AddPromo_MouseUp(object sender, MouseEventArgs e)
        {
            _move = false; // Desactiva el movimiento del formulario
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de precio
        private void txt_pprecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar.IsOnlyNums(); // Verifica si el carácter ingresado es un número
            if (e.Handled) lb_precio.Text = "Solo se permiten numeros."; // Muestra un mensaje si el carácter no es un número
            else lb_precio.Text = ""; // Limpia el mensaje si el carácter es válido
        }

        // Evento que se dispara cuando se presiona una tecla en el campo de nombre de promoción
        private void txt_pname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar.IsOnlyLetter(); // Verifica si el carácter ingresado es una letra
            if (e.Handled) lb_tpromo.Text = "Solo se permiten letras."; // Muestra un mensaje si el carácter no es una letra
            else lb_tpromo.Text = ""; // Limpia el mensaje si el carácter es válido
        }

        // Evento que se dispara cuando se hace clic en el botón de seleccionar promoción
        private void btn_select_Click(object sender, EventArgs e)
        {
            var vp = new ViewPromo(); // Crea una nueva instancia del formulario ViewPromo
            vp.ShowDialog(); // Muestra el formulario de ver promociones como un cuadro de diálogo
            if (Helpers.temppromo != null) // Verifica si se seleccionó una promoción temporal
            {
                currentpromo = Helpers.temppromo; // Asigna la promoción temporal a la variable local
                Helpers.temppromo = null; // Resetea la promoción temporal
                txt_pname.Text = currentpromo!.Nombpromo; // Asigna el nombre de la promoción al campo de texto
                txt_pprecio.Text = currentpromo.Preciopromo.ToString(); // Asigna el precio de la promoción al campo de texto

                // Añade los ingredientes al DataGridView
                foreach (var i in currentpromo.Ingredientes.Split(','))
                {
                    int index = datag_ingredients.Rows.Add(); // Agrega una nueva fila al DataGridView
                    datag_ingredients.Rows[index].Cells[0].Value = i; // Asigna el ingrediente a la celda
                }
                txt_extra.Text = currentpromo!.Extra; // Asigna el extra de la promoción al campo de texto
                btn_add.Enabled = false; // Desactiva el botón de agregar
            }
        }

        // Evento que se dispara cuando se hace clic en el label13 (minimizar ventana)
        private void label13_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized; // Minimiza la ventana
        }

        // Evento que se dispara cuando se hace clic en el botón de agregar promoción
        private void btn_add_Click(object sender, EventArgs e)
        {
            var pro = new Promo(); // Crea una nueva instancia de Promo
            if (!txt_pname.IsNullOrEmpty() && !txt_pprecio.IsNullOrEmpty() && Helpers.TempIngds != null) // Verifica si los campos no están vacíos y hay ingredientes temporales
            {
                if (Helpers.TempIngds!.Count != 0) // Verifica si hay al menos un ingrediente
                {
                    pro.Nombpromo = txt_pname.Text; // Asigna el nombre de la promoción
                    pro.Preciopromo = int.Parse(txt_pprecio.Text); // Asigna el precio de la promoción
                    pro.Ingredientes = string.Join(',', Helpers.TempIngds!); // Asigna los ingredientes de la promoción
                    pro.Extra = txt_extra.Text; // Asigna el extra de la promoción
                    try
                    {
                        Helpers.promos!.AddPromo(pro); // Añade la promoción a la lista de promociones
                        Clear(); // Limpia los campos del formulario
                        MessageBox.Show("Promoción agregada con éxito."); // Muestra un mensaje de éxito
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message, "Ha ocurrido un error"); } // Muestra un mensaje de error en caso de excepción
                }
                else
                    MessageBox.Show("Debe seleccional al menos 1 ingrediente"); // Muestra un mensaje si no hay ingredientes
            }
            else
                MessageBox.Show("Verifique los campos."); // Muestra un mensaje si los campos están vacíos
        }

        // Método para limpiar los campos del formulario
        private void Clear()
        {
            foreach (Control control in this.Controls) // Recorre todos los controles del formulario
            {
                if (control is TextBox textBox) // Si el control es un TextBox
                {
                    textBox.Clear(); // Limpia el texto del TextBox
                }
            }
            Helpers.TempIngds = null; // Resetea los ingredientes temporales
            datag_ingredients.Rows.Clear(); // Limpia las filas del DataGridView
        }

        // Evento que se dispara cuando se hace clic en el botón de seleccionar ingrediente
        private void btn_eselect_Click(object sender, EventArgs e)
        {
            var add = new AddIngredient(); // Crea una nueva instancia del formulario AddIngredient
            add.ShowDialog(); // Muestra el formulario de agregar ingrediente como un cuadro de diálogo
            datag_ingredients.Rows.Clear(); // Limpia las filas del DataGridView
            if (Helpers.TempIngds != null) // Verifica si hay ingredientes temporales
            {
                // Añade los ingredientes temporales al DataGridView
                foreach (var i in Helpers.TempIngds!)
                {
                    int index = datag_ingredients.Rows.Add(); // Agrega una nueva fila al DataGridView
                    datag_ingredients.Rows[index].Cells[0].Value = i; // Asigna el ingrediente a la celda
                }
            }
        }
    }
}
