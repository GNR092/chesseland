﻿namespace Chesseland
{
    partial class AddPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPromo));
            label1 = new Label();
            label2 = new Label();
            textBox1 = new TextBox();
            textBox2 = new TextBox();
            label3 = new Label();
            label4 = new Label();
            dataGridView1 = new DataGridView();
            label5 = new Label();
            textBox3 = new TextBox();
            label6 = new Label();
            txt_pprecio = new TextBox();
            txt_pname = new TextBox();
            label9 = new Label();
            label10 = new Label();
            txt_extra = new TextBox();
            col_ingredients = new DataGridViewTextBoxColumn();
            datag_ingredients = new DataGridView();
            pictureBox1 = new PictureBox();
            label11 = new Label();
            label12 = new Label();
            pictureBox4 = new PictureBox();
            btn_exit = new Button();
            label13 = new Label();
            pictureBox2 = new PictureBox();
            label14 = new Label();
            label8 = new Label();
            label15 = new Label();
            btn_select = new Button();
            btn_eselect = new Button();
            btn_back = new Button();
            btn_add = new Button();
            btn_mod = new Button();
            pictureBox3 = new PictureBox();
            lb_precio = new Label();
            lb_tpromo = new Label();
            button1 = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)datag_ingredients).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 9);
            label1.Name = "label1";
            label1.Size = new Size(95, 20);
            label1.TabIndex = 0;
            label1.Text = "Promociones";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(50, 68);
            label2.Name = "label2";
            label2.Size = new Size(150, 20);
            label2.TabIndex = 1;
            label2.Text = "Nombre de producto";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(214, 65);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(125, 23);
            textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(214, 98);
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(125, 23);
            textBox2.TabIndex = 4;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(50, 101);
            label3.Name = "label3";
            label3.Size = new Size(50, 20);
            label3.TabIndex = 3;
            label3.Text = "Precio";
            // 
            // label4
            // 
            label4.Location = new Point(0, 0);
            label4.Name = "label4";
            label4.Size = new Size(100, 23);
            label4.TabIndex = 0;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeight = 29;
            dataGridView1.Location = new Point(0, 0);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new Size(240, 150);
            dataGridView1.TabIndex = 0;
            // 
            // label5
            // 
            label5.Location = new Point(0, 0);
            label5.Name = "label5";
            label5.Size = new Size(100, 23);
            label5.TabIndex = 0;
            // 
            // textBox3
            // 
            textBox3.Location = new Point(0, 0);
            textBox3.Name = "textBox3";
            textBox3.Size = new Size(100, 23);
            textBox3.TabIndex = 0;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(41, 413);
            label6.Name = "label6";
            label6.Size = new Size(52, 17);
            label6.TabIndex = 19;
            label6.Text = "Extras:";
            // 
            // txt_pprecio
            // 
            txt_pprecio.Location = new Point(151, 171);
            txt_pprecio.Name = "txt_pprecio";
            txt_pprecio.Size = new Size(300, 23);
            txt_pprecio.TabIndex = 16;
            txt_pprecio.KeyPress += txt_pprecio_KeyPress;
            // 
            // txt_pname
            // 
            txt_pname.Location = new Point(151, 119);
            txt_pname.Name = "txt_pname";
            txt_pname.Size = new Size(300, 23);
            txt_pname.TabIndex = 14;
            txt_pname.KeyPress += txt_pname_KeyPress;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(41, 121);
            label9.Name = "label9";
            label9.Size = new Size(54, 17);
            label9.TabIndex = 13;
            label9.Text = "Promo:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new Point(9, 7);
            label10.Name = "label10";
            label10.Size = new Size(77, 15);
            label10.TabIndex = 12;
            label10.Text = "Promociones";
            // 
            // txt_extra
            // 
            txt_extra.Location = new Point(151, 370);
            txt_extra.Multiline = true;
            txt_extra.Name = "txt_extra";
            txt_extra.Size = new Size(300, 112);
            txt_extra.TabIndex = 20;
            // 
            // col_ingredients
            // 
            col_ingredients.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col_ingredients.HeaderText = "";
            col_ingredients.MinimumWidth = 6;
            col_ingredients.Name = "col_ingredients";
            col_ingredients.ReadOnly = true;
            col_ingredients.Resizable = DataGridViewTriState.False;
            // 
            // datag_ingredients
            // 
            datag_ingredients.AllowUserToAddRows = false;
            datag_ingredients.AllowUserToDeleteRows = false;
            datag_ingredients.BackgroundColor = Color.FromArgb(255, 224, 192);
            datag_ingredients.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            datag_ingredients.Columns.AddRange(new DataGridViewColumn[] { col_ingredients });
            datag_ingredients.GridColor = Color.FromArgb(255, 128, 0);
            datag_ingredients.Location = new Point(151, 219);
            datag_ingredients.Name = "datag_ingredients";
            datag_ingredients.ReadOnly = true;
            datag_ingredients.RowHeadersWidth = 51;
            datag_ingredients.RowTemplate.Height = 29;
            datag_ingredients.Size = new Size(418, 145);
            datag_ingredients.TabIndex = 24;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox1.Location = new Point(-2, 25);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(625, 20);
            pictureBox1.TabIndex = 27;
            pictureBox1.TabStop = false;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Roboto", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(228, 60);
            label11.Name = "label11";
            label11.Size = new Size(0, 27);
            label11.TabIndex = 28;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(500, 52);
            label12.Name = "label12";
            label12.Size = new Size(69, 14);
            label12.TabIndex = 29;
            label12.Text = "Chesseland";
            // 
            // pictureBox4
            // 
            pictureBox4.Image = (Image)resources.GetObject("pictureBox4.Image");
            pictureBox4.Location = new Point(573, 48);
            pictureBox4.Name = "pictureBox4";
            pictureBox4.Size = new Size(27, 29);
            pictureBox4.TabIndex = 30;
            pictureBox4.TabStop = false;
            // 
            // btn_exit
            // 
            btn_exit.BackColor = Color.Crimson;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.Location = new Point(589, 23);
            btn_exit.Margin = new Padding(3, 2, 3, 2);
            btn_exit.Name = "btn_exit";
            btn_exit.Size = new Size(21, 20);
            btn_exit.TabIndex = 31;
            btn_exit.Text = "X";
            btn_exit.UseVisualStyleBackColor = false;
            btn_exit.Click += btn_inicio_Click;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.BackColor = Color.White;
            label13.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label13.Location = new Point(570, 18);
            label13.Name = "label13";
            label13.Size = new Size(18, 27);
            label13.TabIndex = 32;
            label13.Text = "-";
            label13.Click += label13_Click;
            // 
            // pictureBox2
            // 
            pictureBox2.Image = Properties.Resources.descargar;
            pictureBox2.Location = new Point(9, 48);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(185, 50);
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.TabIndex = 33;
            pictureBox2.TabStop = false;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Font = new Font("Roboto", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point);
            label14.Location = new Point(234, 65);
            label14.Name = "label14";
            label14.Size = new Size(207, 33);
            label14.TabIndex = 34;
            label14.Text = "PROMOCIONES";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label8.Location = new Point(44, 177);
            label8.Name = "label8";
            label8.Size = new Size(53, 17);
            label8.TabIndex = 35;
            label8.Text = "Precio:";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(41, 208);
            label15.Name = "label15";
            label15.Size = new Size(93, 17);
            label15.TabIndex = 36;
            label15.Text = "Ingredientes:";
            // 
            // btn_select
            // 
            btn_select.BackColor = Color.Goldenrod;
            btn_select.FlatStyle = FlatStyle.Flat;
            btn_select.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_select.Location = new Point(478, 121);
            btn_select.Margin = new Padding(3, 2, 3, 2);
            btn_select.Name = "btn_select";
            btn_select.Size = new Size(110, 32);
            btn_select.TabIndex = 37;
            btn_select.Text = "SELECCIONAR";
            btn_select.UseVisualStyleBackColor = false;
            btn_select.Click += btn_select_Click;
            // 
            // btn_eselect
            // 
            btn_eselect.BackColor = Color.Goldenrod;
            btn_eselect.FlatStyle = FlatStyle.Flat;
            btn_eselect.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_eselect.Location = new Point(478, 370);
            btn_eselect.Margin = new Padding(3, 2, 3, 2);
            btn_eselect.Name = "btn_eselect";
            btn_eselect.Size = new Size(110, 40);
            btn_eselect.TabIndex = 38;
            btn_eselect.Text = "SELECCIONAR ING";
            btn_eselect.UseVisualStyleBackColor = false;
            btn_eselect.Click += btn_eselect_Click;
            // 
            // btn_back
            // 
            btn_back.BackColor = Color.DarkOrange;
            btn_back.FlatStyle = FlatStyle.Flat;
            btn_back.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_back.Location = new Point(117, 510);
            btn_back.Margin = new Padding(3, 2, 3, 2);
            btn_back.Name = "btn_back";
            btn_back.Size = new Size(110, 30);
            btn_back.TabIndex = 39;
            btn_back.Text = "REGRESAR";
            btn_back.UseVisualStyleBackColor = false;
            btn_back.Click += btn_inicio_Click;
            // 
            // btn_add
            // 
            btn_add.BackColor = Color.Goldenrod;
            btn_add.FlatStyle = FlatStyle.Flat;
            btn_add.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_add.Location = new Point(280, 510);
            btn_add.Margin = new Padding(3, 2, 3, 2);
            btn_add.Name = "btn_add";
            btn_add.Size = new Size(110, 30);
            btn_add.TabIndex = 40;
            btn_add.Text = "AGREGAR";
            btn_add.UseVisualStyleBackColor = false;
            btn_add.Click += btn_add_Click;
            // 
            // btn_mod
            // 
            btn_mod.BackColor = Color.DarkOrange;
            btn_mod.FlatStyle = FlatStyle.Flat;
            btn_mod.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_mod.Location = new Point(449, 510);
            btn_mod.Margin = new Padding(3, 2, 3, 2);
            btn_mod.Name = "btn_mod";
            btn_mod.Size = new Size(110, 30);
            btn_mod.TabIndex = 41;
            btn_mod.Text = "MODIFICAR";
            btn_mod.UseVisualStyleBackColor = false;
            // 
            // pictureBox3
            // 
            pictureBox3.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox3.Location = new Point(-2, 558);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(625, 20);
            pictureBox3.TabIndex = 42;
            pictureBox3.TabStop = false;
            // 
            // lb_precio
            // 
            lb_precio.AutoSize = true;
            lb_precio.Font = new Font("Consolas", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lb_precio.ForeColor = Color.Red;
            lb_precio.Location = new Point(151, 197);
            lb_precio.Name = "lb_precio";
            lb_precio.Size = new Size(0, 14);
            lb_precio.TabIndex = 43;
            // 
            // lb_tpromo
            // 
            lb_tpromo.AutoSize = true;
            lb_tpromo.Font = new Font("Consolas", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lb_tpromo.ForeColor = Color.Red;
            lb_tpromo.Location = new Point(151, 145);
            lb_tpromo.Name = "lb_tpromo";
            lb_tpromo.Size = new Size(0, 14);
            lb_tpromo.TabIndex = 44;
            // 
            // button1
            // 
            button1.BackColor = Color.Goldenrod;
            button1.FlatStyle = FlatStyle.Flat;
            button1.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            button1.Location = new Point(478, 429);
            button1.Margin = new Padding(3, 2, 3, 2);
            button1.Name = "button1";
            button1.Size = new Size(110, 29);
            button1.TabIndex = 45;
            button1.Text = "ELIMINAR ING";
            button1.UseVisualStyleBackColor = false;
            // 
            // AddPromo
            // 
            ClientSize = new Size(622, 578);
            Controls.Add(button1);
            Controls.Add(lb_tpromo);
            Controls.Add(lb_precio);
            Controls.Add(pictureBox3);
            Controls.Add(btn_mod);
            Controls.Add(btn_add);
            Controls.Add(btn_back);
            Controls.Add(btn_eselect);
            Controls.Add(btn_select);
            Controls.Add(label15);
            Controls.Add(label8);
            Controls.Add(label14);
            Controls.Add(pictureBox2);
            Controls.Add(label13);
            Controls.Add(btn_exit);
            Controls.Add(pictureBox4);
            Controls.Add(label12);
            Controls.Add(label11);
            Controls.Add(pictureBox1);
            Controls.Add(datag_ingredients);
            Controls.Add(txt_extra);
            Controls.Add(label6);
            Controls.Add(txt_pprecio);
            Controls.Add(txt_pname);
            Controls.Add(label9);
            Controls.Add(label10);
            FormBorderStyle = FormBorderStyle.None;
            Name = "AddPromo";
            StartPosition = FormStartPosition.CenterScreen;
            TopMost = true;
            MouseDown += AddPromo_MouseDown;
            MouseMove += AddPromo_MouseMove;
            MouseUp += AddPromo_MouseUp;
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)datag_ingredients).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox textBox1;
        private TextBox textBox2;
        private Label label3;
        private Label label4;
        private DataGridView dataGridView1;
        private Label label5;
        private TextBox textBox3;
        private Label label6;
        private TextBox txt_pprecio;
        private TextBox txt_pname;
        private Label label9;
        private Label label10;
        private TextBox txt_extra;
        private DataGridViewTextBoxColumn col_ingredients;
        private DataGridView datag_ingredients;
        private PictureBox pictureBox1;
        private Label label11;
        private Label label12;
        private PictureBox pictureBox4;
        private Button btn_exit;
        private Label label13;
        private PictureBox pictureBox2;
        private Label label14;
        private Label label8;
        private Label label15;
        private Button btn_select;
        private Button btn_eselect;
        private Button btn_back;
        private Button btn_add;
        private Button btn_mod;
        private PictureBox pictureBox3;
        private Label lb_precio;
        private Label lb_tpromo;
        private Button button1;
    }
}