﻿namespace Chesseland
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            lb_welcome = new Label();
            btn_promo = new Button();
            btn_products = new Button();
            btn_sells = new Button();
            btn_actua = new Button();
            btn_search = new Button();
            dataGridView1 = new DataGridView();
            c_folio = new DataGridViewTextBoxColumn();
            c_comedor = new DataGridViewTextBoxColumn();
            Estado = new DataGridViewTextBoxColumn();
            ms_barra = new MenuStrip();
            administracionToolStripMenuItem = new ToolStripMenuItem();
            agregarUsuariosToolStripMenuItem = new ToolStripMenuItem();
            agregarToolStripMenuItem = new ToolStripMenuItem();
            eliminarToolStripMenuItem = new ToolStripMenuItem();
            dateTimePicker1 = new DateTimePicker();
            label1 = new Label();
            label2 = new Label();
            btn_exit = new Button();
            label4 = new Label();
            label3 = new Label();
            label5 = new Label();
            pictureBox1 = new PictureBox();
            pictureBox2 = new PictureBox();
            label6 = new Label();
            pictureBox4 = new PictureBox();
            pictureBox3 = new PictureBox();
            pictureBox5 = new PictureBox();
            textBox6 = new TextBox();
            btn_logout = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            ms_barra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).BeginInit();
            SuspendLayout();
            // 
            // lb_welcome
            // 
            lb_welcome.AutoSize = true;
            lb_welcome.Font = new Font("Roboto", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lb_welcome.Location = new Point(47, 158);
            lb_welcome.Name = "lb_welcome";
            lb_welcome.Size = new Size(203, 19);
            lb_welcome.TabIndex = 0;
            lb_welcome.Text = "Bienvenido:   %username%";
            // 
            // btn_promo
            // 
            btn_promo.BackColor = Color.FromArgb(255, 192, 128);
            btn_promo.FlatStyle = FlatStyle.Flat;
            btn_promo.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_promo.Location = new Point(47, 274);
            btn_promo.Margin = new Padding(3, 2, 3, 2);
            btn_promo.Name = "btn_promo";
            btn_promo.Size = new Size(135, 22);
            btn_promo.TabIndex = 1;
            btn_promo.Text = "PROMOCIONES";
            btn_promo.UseVisualStyleBackColor = false;
            btn_promo.Click += btn_promo_Click;
            // 
            // btn_products
            // 
            btn_products.BackColor = Color.FromArgb(255, 192, 128);
            btn_products.FlatStyle = FlatStyle.Flat;
            btn_products.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_products.Location = new Point(47, 300);
            btn_products.Margin = new Padding(3, 2, 3, 2);
            btn_products.Name = "btn_products";
            btn_products.Size = new Size(135, 22);
            btn_products.TabIndex = 2;
            btn_products.Text = "PRODUCTOS";
            btn_products.UseVisualStyleBackColor = false;
            btn_products.Click += btn_products_Click;
            // 
            // btn_sells
            // 
            btn_sells.BackColor = Color.FromArgb(255, 192, 128);
            btn_sells.FlatStyle = FlatStyle.Flat;
            btn_sells.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_sells.Location = new Point(47, 326);
            btn_sells.Margin = new Padding(3, 2, 3, 2);
            btn_sells.Name = "btn_sells";
            btn_sells.Size = new Size(135, 22);
            btn_sells.TabIndex = 3;
            btn_sells.Text = "VENTAS";
            btn_sells.UseVisualStyleBackColor = false;
            btn_sells.Click += btn_sells_Click;
            // 
            // btn_actua
            // 
            btn_actua.BackColor = Color.FromArgb(255, 192, 128);
            btn_actua.FlatStyle = FlatStyle.Flat;
            btn_actua.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_actua.Location = new Point(47, 352);
            btn_actua.Margin = new Padding(3, 2, 3, 2);
            btn_actua.Name = "btn_actua";
            btn_actua.Size = new Size(135, 22);
            btn_actua.TabIndex = 4;
            btn_actua.Text = "ACTUALIZACIONES";
            btn_actua.UseVisualStyleBackColor = false;
            btn_actua.Visible = false;
            // 
            // btn_search
            // 
            btn_search.BackColor = Color.DarkOrange;
            btn_search.FlatStyle = FlatStyle.Flat;
            btn_search.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_search.Location = new Point(751, 484);
            btn_search.Margin = new Padding(3, 2, 3, 2);
            btn_search.Name = "btn_search";
            btn_search.Size = new Size(110, 22);
            btn_search.TabIndex = 5;
            btn_search.Text = "BUSCAR";
            btn_search.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.BackgroundColor = Color.FromArgb(255, 224, 192);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.RaisedVertical;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { c_folio, c_comedor, Estado });
            dataGridView1.GridColor = Color.FromArgb(255, 128, 0);
            dataGridView1.Location = new Point(181, 197);
            dataGridView1.Margin = new Padding(3, 2, 3, 2);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.ReadOnly = true;
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.RowTemplate.Height = 29;
            dataGridView1.Size = new Size(655, 270);
            dataGridView1.TabIndex = 6;
            // 
            // c_folio
            // 
            c_folio.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_folio.HeaderText = "Folio";
            c_folio.MinimumWidth = 6;
            c_folio.Name = "c_folio";
            c_folio.ReadOnly = true;
            // 
            // c_comedor
            // 
            c_comedor.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_comedor.HeaderText = "Mesa";
            c_comedor.MinimumWidth = 6;
            c_comedor.Name = "c_comedor";
            c_comedor.ReadOnly = true;
            // 
            // Estado
            // 
            Estado.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            Estado.HeaderText = "Estado";
            Estado.MinimumWidth = 6;
            Estado.Name = "Estado";
            Estado.ReadOnly = true;
            // 
            // ms_barra
            // 
            ms_barra.Items.AddRange(new ToolStripItem[] { administracionToolStripMenuItem });
            ms_barra.Location = new Point(0, 0);
            ms_barra.Name = "ms_barra";
            ms_barra.Size = new Size(884, 24);
            ms_barra.TabIndex = 7;
            ms_barra.Text = "menuStrip1";
            // 
            // administracionToolStripMenuItem
            // 
            administracionToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { agregarUsuariosToolStripMenuItem });
            administracionToolStripMenuItem.Name = "administracionToolStripMenuItem";
            administracionToolStripMenuItem.Size = new Size(100, 20);
            administracionToolStripMenuItem.Text = "Administración";
            // 
            // agregarUsuariosToolStripMenuItem
            // 
            agregarUsuariosToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { agregarToolStripMenuItem, eliminarToolStripMenuItem });
            agregarUsuariosToolStripMenuItem.Name = "agregarUsuariosToolStripMenuItem";
            agregarUsuariosToolStripMenuItem.Size = new Size(180, 22);
            agregarUsuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // agregarToolStripMenuItem
            // 
            agregarToolStripMenuItem.Name = "agregarToolStripMenuItem";
            agregarToolStripMenuItem.Size = new Size(180, 22);
            agregarToolStripMenuItem.Text = "Agregar";
            agregarToolStripMenuItem.Click += agregarToolStripMenuItem_Click;
            // 
            // eliminarToolStripMenuItem
            // 
            eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            eliminarToolStripMenuItem.Size = new Size(180, 22);
            eliminarToolStripMenuItem.Text = "Modificar/Eliminar";
            eliminarToolStripMenuItem.Click += eliminarToolStripMenuItem_Click;
            // 
            // dateTimePicker1
            // 
            dateTimePicker1.CalendarFont = new Font("Roboto", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dateTimePicker1.Location = new Point(12, 53);
            dateTimePicker1.Name = "dateTimePicker1";
            dateTimePicker1.Size = new Size(230, 23);
            dateTimePicker1.TabIndex = 8;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(751, 62);
            label1.Name = "label1";
            label1.Size = new Size(69, 14);
            label1.TabIndex = 9;
            label1.Text = "Chesseland";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(194, 73);
            label2.Name = "label2";
            label2.Size = new Size(0, 15);
            label2.TabIndex = 10;
            // 
            // btn_exit
            // 
            btn_exit.BackColor = Color.Crimson;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.Location = new Point(751, 27);
            btn_exit.Margin = new Padding(3, 2, 3, 2);
            btn_exit.Name = "btn_exit";
            btn_exit.Size = new Size(21, 20);
            btn_exit.TabIndex = 11;
            btn_exit.Text = "X";
            btn_exit.UseVisualStyleBackColor = false;
            btn_exit.Click += btn_exit_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = Color.White;
            label4.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(727, 25);
            label4.Name = "label4";
            label4.Size = new Size(18, 27);
            label4.TabIndex = 12;
            label4.Text = "-";
            label4.Click += label4_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(413, 133);
            label3.Name = "label3";
            label3.Size = new Size(0, 27);
            label3.TabIndex = 13;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(404, 133);
            label5.Name = "label5";
            label5.Size = new Size(0, 15);
            label5.TabIndex = 14;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox1.Location = new Point(0, 27);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(625, 20);
            pictureBox1.TabIndex = 15;
            pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            pictureBox2.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox2.Location = new Point(612, 27);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(261, 20);
            pictureBox2.TabIndex = 16;
            pictureBox2.TabStop = false;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Roboto", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(33, 92);
            label6.Name = "label6";
            label6.Size = new Size(95, 33);
            label6.TabIndex = 17;
            label6.Text = "INICIO";
            label6.Click += label6_Click;
            // 
            // pictureBox4
            // 
            pictureBox4.Image = (Image)resources.GetObject("pictureBox4.Image");
            pictureBox4.Location = new Point(826, 59);
            pictureBox4.Name = "pictureBox4";
            pictureBox4.Size = new Size(27, 29);
            pictureBox4.TabIndex = 19;
            pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            pictureBox3.Image = Properties.Resources.descargar;
            pictureBox3.Location = new Point(404, 59);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(278, 71);
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.TabIndex = 34;
            pictureBox3.TabStop = false;
            // 
            // pictureBox5
            // 
            pictureBox5.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox5.Location = new Point(460, 27);
            pictureBox5.Name = "pictureBox5";
            pictureBox5.Size = new Size(261, 20);
            pictureBox5.TabIndex = 35;
            pictureBox5.TabStop = false;
            // 
            // textBox6
            // 
            textBox6.Location = new Point(272, 154);
            textBox6.Name = "textBox6";
            textBox6.Size = new Size(564, 23);
            textBox6.TabIndex = 36;
            // 
            // btn_logout
            // 
            btn_logout.BackColor = Color.DarkOrange;
            btn_logout.FlatStyle = FlatStyle.Flat;
            btn_logout.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_logout.Location = new Point(743, 92);
            btn_logout.Margin = new Padding(3, 2, 3, 2);
            btn_logout.Name = "btn_logout";
            btn_logout.Size = new Size(110, 22);
            btn_logout.TabIndex = 37;
            btn_logout.Text = "Cerrar Session";
            btn_logout.UseVisualStyleBackColor = false;
            btn_logout.Click += btn_logout_Click;
            // 
            // Main
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(884, 517);
            Controls.Add(btn_logout);
            Controls.Add(textBox6);
            Controls.Add(pictureBox5);
            Controls.Add(pictureBox3);
            Controls.Add(pictureBox4);
            Controls.Add(label6);
            Controls.Add(label4);
            Controls.Add(btn_exit);
            Controls.Add(pictureBox2);
            Controls.Add(pictureBox1);
            Controls.Add(label5);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(dateTimePicker1);
            Controls.Add(dataGridView1);
            Controls.Add(btn_search);
            Controls.Add(btn_actua);
            Controls.Add(btn_sells);
            Controls.Add(btn_products);
            Controls.Add(btn_promo);
            Controls.Add(lb_welcome);
            Controls.Add(ms_barra);
            FormBorderStyle = FormBorderStyle.None;
            MainMenuStrip = ms_barra;
            Margin = new Padding(3, 2, 3, 2);
            Name = "Main";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "ChesseLand-Control";
            Load += Main_Load;
            MouseDown += Main_MouseDown;
            MouseMove += Main_MouseMove;
            MouseUp += Main_MouseUp;
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ms_barra.ResumeLayout(false);
            ms_barra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lb_welcome;
        private Button btn_promo;
        private Button btn_products;
        private Button btn_sells;
        private Button btn_actua;
        private Button btn_search;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn c_folio;
        private DataGridViewTextBoxColumn c_comedor;
        private DataGridViewTextBoxColumn Estado;
        private MenuStrip ms_barra;
        private ToolStripMenuItem administracionToolStripMenuItem;
        private ToolStripMenuItem agregarUsuariosToolStripMenuItem;
        private ToolStripMenuItem agregarToolStripMenuItem;
        private ToolStripMenuItem eliminarToolStripMenuItem;
        private DateTimePicker dateTimePicker1;
        private Label label1;
        private Label label2;
        private Button btn_exit;
        private Label label4;
        private Label label3;
        private Label label5;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Label label6;
        private PictureBox pictureBox4;
        private PictureBox pictureBox3;
        private PictureBox pictureBox5;
        private TextBox textBox6;
        private Button btn_logout;
    }
}