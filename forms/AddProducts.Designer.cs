﻿namespace Chesseland
{
    partial class AddProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                Helpers.TempIngds = null;
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProducts));
            label1 = new Label();
            label2 = new Label();
            txt_product = new TextBox();
            txt_precio = new TextBox();
            label3 = new Label();
            label4 = new Label();
            dataGridIngredientes = new DataGridView();
            col_ingredients = new DataGridViewTextBoxColumn();
            pictureBox1 = new PictureBox();
            label5 = new Label();
            pictureBox4 = new PictureBox();
            btn_exit = new Button();
            label6 = new Label();
            label7 = new Label();
            btn_search = new Button();
            btn_add = new Button();
            btn_back = new Button();
            lb_product = new Label();
            lb_precio = new Label();
            btn_sing = new Button();
            btn_deling = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridIngredientes).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(10, 7);
            label1.Name = "label1";
            label1.Size = new Size(101, 15);
            label1.TabIndex = 0;
            label1.Text = "Agregar Producto";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(52, 132);
            label2.Name = "label2";
            label2.Size = new Size(70, 17);
            label2.TabIndex = 1;
            label2.Text = "Producto:";
            // 
            // txt_product
            // 
            txt_product.Location = new Point(141, 124);
            txt_product.Margin = new Padding(3, 2, 3, 2);
            txt_product.Name = "txt_product";
            txt_product.Size = new Size(358, 23);
            txt_product.TabIndex = 2;
            txt_product.KeyPress += txt_product_KeyPress;
            // 
            // txt_precio
            // 
            txt_precio.Location = new Point(142, 168);
            txt_precio.Margin = new Padding(3, 2, 3, 2);
            txt_precio.Name = "txt_precio";
            txt_precio.Size = new Size(158, 23);
            txt_precio.TabIndex = 4;
            txt_precio.KeyPress += txt_precio_KeyPress;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(52, 176);
            label3.Name = "label3";
            label3.Size = new Size(53, 17);
            label3.TabIndex = 3;
            label3.Text = "Precio:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Roboto", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(52, 207);
            label4.Name = "label4";
            label4.Size = new Size(93, 17);
            label4.TabIndex = 5;
            label4.Text = "Ingredientes:";
            // 
            // dataGridIngredientes
            // 
            dataGridViewCellStyle1.BackColor = Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new Font("Roboto", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = Color.FromArgb(255, 192, 128);
            dataGridViewCellStyle1.SelectionForeColor = Color.FromArgb(255, 128, 0);
            dataGridIngredientes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridIngredientes.BackgroundColor = Color.FromArgb(255, 224, 192);
            dataGridIngredientes.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridIngredientes.Columns.AddRange(new DataGridViewColumn[] { col_ingredients });
            dataGridIngredientes.GridColor = Color.FromArgb(255, 128, 0);
            dataGridIngredientes.Location = new Point(150, 238);
            dataGridIngredientes.Margin = new Padding(3, 2, 3, 2);
            dataGridIngredientes.Name = "dataGridIngredientes";
            dataGridIngredientes.ReadOnly = true;
            dataGridIngredientes.RowHeadersWidth = 51;
            dataGridIngredientes.RowTemplate.Height = 29;
            dataGridIngredientes.Size = new Size(397, 230);
            dataGridIngredientes.TabIndex = 6;
            // 
            // col_ingredients
            // 
            col_ingredients.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            col_ingredients.HeaderText = "";
            col_ingredients.MinimumWidth = 6;
            col_ingredients.Name = "col_ingredients";
            col_ingredients.ReadOnly = true;
            col_ingredients.Resizable = DataGridViewTriState.False;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox1.Location = new Point(-3, 25);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(625, 20);
            pictureBox1.TabIndex = 16;
            pictureBox1.TabStop = false;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(488, 55);
            label5.Name = "label5";
            label5.Size = new Size(69, 14);
            label5.TabIndex = 17;
            label5.Text = "Chesseland";
            // 
            // pictureBox4
            // 
            pictureBox4.Image = (Image)resources.GetObject("pictureBox4.Image");
            pictureBox4.Location = new Point(565, 48);
            pictureBox4.Name = "pictureBox4";
            pictureBox4.Size = new Size(27, 29);
            pictureBox4.TabIndex = 20;
            pictureBox4.TabStop = false;
            // 
            // btn_exit
            // 
            btn_exit.BackColor = Color.Crimson;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.Location = new Point(587, 25);
            btn_exit.Margin = new Padding(3, 2, 3, 2);
            btn_exit.Name = "btn_exit";
            btn_exit.Size = new Size(21, 20);
            btn_exit.TabIndex = 21;
            btn_exit.Text = "X";
            btn_exit.UseVisualStyleBackColor = false;
            btn_exit.Click += btn_exit_Click;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = Color.White;
            label6.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(569, 18);
            label6.Name = "label6";
            label6.Size = new Size(18, 27);
            label6.TabIndex = 22;
            label6.Text = "-";
            label6.Click += label6_Click;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Roboto", 22.2F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(239, 68);
            label7.Name = "label7";
            label7.Size = new Size(194, 37);
            label7.TabIndex = 23;
            label7.Text = "PRODUCTOS";
            // 
            // btn_search
            // 
            btn_search.BackColor = Color.Goldenrod;
            btn_search.FlatStyle = FlatStyle.Flat;
            btn_search.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_search.Location = new Point(418, 509);
            btn_search.Margin = new Padding(3, 2, 3, 2);
            btn_search.Name = "btn_search";
            btn_search.Size = new Size(110, 22);
            btn_search.TabIndex = 25;
            btn_search.Text = "BUSCAR";
            btn_search.UseVisualStyleBackColor = false;
            btn_search.Click += btn_search_Click;
            // 
            // btn_add
            // 
            btn_add.BackColor = Color.DarkOrange;
            btn_add.FlatStyle = FlatStyle.Flat;
            btn_add.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_add.Location = new Point(281, 509);
            btn_add.Margin = new Padding(3, 2, 3, 2);
            btn_add.Name = "btn_add";
            btn_add.Size = new Size(110, 22);
            btn_add.TabIndex = 26;
            btn_add.Text = "AGREGAR";
            btn_add.UseVisualStyleBackColor = false;
            btn_add.Click += btn_add_Click;
            // 
            // btn_back
            // 
            btn_back.BackColor = Color.Goldenrod;
            btn_back.FlatStyle = FlatStyle.Flat;
            btn_back.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_back.ImageAlign = ContentAlignment.BottomLeft;
            btn_back.Location = new Point(141, 509);
            btn_back.Margin = new Padding(3, 2, 3, 2);
            btn_back.Name = "btn_back";
            btn_back.Size = new Size(110, 22);
            btn_back.TabIndex = 27;
            btn_back.Text = "REGRESAR";
            btn_back.UseVisualStyleBackColor = false;
            btn_back.Click += btn_exit_Click;
            // 
            // lb_product
            // 
            lb_product.AutoSize = true;
            lb_product.Font = new Font("Consolas", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lb_product.ForeColor = Color.Red;
            lb_product.Location = new Point(145, 151);
            lb_product.Name = "lb_product";
            lb_product.Size = new Size(0, 14);
            lb_product.TabIndex = 28;
            // 
            // lb_precio
            // 
            lb_precio.AutoSize = true;
            lb_precio.Font = new Font("Consolas", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lb_precio.ForeColor = Color.Red;
            lb_precio.Location = new Point(145, 195);
            lb_precio.Name = "lb_precio";
            lb_precio.Size = new Size(0, 14);
            lb_precio.TabIndex = 29;
            // 
            // btn_sing
            // 
            btn_sing.BackColor = Color.Goldenrod;
            btn_sing.FlatStyle = FlatStyle.Flat;
            btn_sing.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_sing.Location = new Point(150, 472);
            btn_sing.Margin = new Padding(3, 2, 3, 2);
            btn_sing.Name = "btn_sing";
            btn_sing.Size = new Size(179, 22);
            btn_sing.TabIndex = 30;
            btn_sing.Text = "Seleccionar ingredientes";
            btn_sing.UseVisualStyleBackColor = false;
            btn_sing.Click += btn_sing_Click;
            // 
            // btn_deling
            // 
            btn_deling.BackColor = Color.Goldenrod;
            btn_deling.FlatStyle = FlatStyle.Flat;
            btn_deling.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_deling.Location = new Point(390, 472);
            btn_deling.Margin = new Padding(3, 2, 3, 2);
            btn_deling.Name = "btn_deling";
            btn_deling.Size = new Size(157, 22);
            btn_deling.TabIndex = 31;
            btn_deling.Text = "Eliminar ingrediente";
            btn_deling.UseVisualStyleBackColor = false;
            btn_deling.Click += btn_deling_Click;
            // 
            // AddProducts
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(621, 580);
            Controls.Add(btn_deling);
            Controls.Add(btn_sing);
            Controls.Add(lb_precio);
            Controls.Add(lb_product);
            Controls.Add(btn_back);
            Controls.Add(btn_add);
            Controls.Add(btn_search);
            Controls.Add(label7);
            Controls.Add(label6);
            Controls.Add(btn_exit);
            Controls.Add(pictureBox4);
            Controls.Add(label5);
            Controls.Add(pictureBox1);
            Controls.Add(dataGridIngredientes);
            Controls.Add(label4);
            Controls.Add(txt_precio);
            Controls.Add(label3);
            Controls.Add(txt_product);
            Controls.Add(label2);
            Controls.Add(label1);
            FormBorderStyle = FormBorderStyle.None;
            Margin = new Padding(3, 2, 3, 2);
            Name = "AddProducts";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "AddProducts";
            TopMost = true;
            TransparencyKey = Color.SandyBrown;
            MouseDown += AddProducts_MouseDown;
            MouseMove += AddProducts_MouseMove;
            MouseUp += AddProducts_MouseUp;
            ((System.ComponentModel.ISupportInitialize)dataGridIngredientes).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox txt_product;
        private TextBox txt_precio;
        private Label label3;
        private Label label4;
        private DataGridView dataGridIngredientes;
        private DataGridViewTextBoxColumn col_ingredients;
        private PictureBox pictureBox1;
        private Label label5;
        private PictureBox pictureBox4;
        private Button btn_exit;
        private Label label6;
        private Label label7;
#pragma warning disable CS0169 // El campo 'AddProducts.pictureBox2' nunca se usa
        private PictureBox pictureBox2;
#pragma warning restore CS0169 // El campo 'AddProducts.pictureBox2' nunca se usa
        private Button btn_search;
        private Button btn_add;
        private Button btn_back;
#pragma warning disable CS0169 // El campo 'AddProducts.pictureBox3' nunca se usa
        private PictureBox pictureBox3;
#pragma warning restore CS0169 // El campo 'AddProducts.pictureBox3' nunca se usa
        private Label lb_product;
        private Label lb_precio;
        private Button btn_sing;
        private Button btn_deling;
    }
}