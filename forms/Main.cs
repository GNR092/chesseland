using Chesseland.forms; // Importa el namespace Chesseland.forms

namespace Chesseland
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Main : Form
    {
        private bool _move = false; // Variable para controlar el movimiento del formulario
        private int mx, my; // Variables para almacenar las coordenadas del rat�n

        /// <summary>
        /// 
        /// </summary>
        public Main()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
        }

        // Evento que se dispara cuando se hace clic en el bot�n de promoci�n
        private void btn_promo_Click(object sender, EventArgs e)
        {
            var addpromo = new AddPromo(); // Crea una nueva instancia del formulario AddPromo
            addpromo.Show(); // Muestra el formulario de promoci�n
        }

        // Evento que se dispara cuando se hace clic en el bot�n de productos
        private void btn_products_Click(object sender, EventArgs e)
        {
            var addproducts = new AddProducts(); // Crea una nueva instancia del formulario AddProducts
            addproducts.Show(); // Muestra el formulario de productos
        }

        // Evento que se dispara cuando se hace clic en el bot�n de ventas
        private void btn_sells_Click(object sender, EventArgs e)
        {
            var ventas = new Ventas(); // Crea una nueva instancia del formulario Ventas
            ventas.Show(); // Muestra el formulario de ventas
        }

        // Evento que se dispara cuando el formulario se carga
        private void Main_Load(object sender, EventArgs e)
        {
            if (Helpers.CurrentUser?.Rol == Roles.Superadmin || Helpers.CurrentUser?.Rol == Roles.Admin) // Verifica el rol del usuario
            {
                lb_welcome.ForeColor = Color.Red; // Cambia el color del texto a rojo
                lb_welcome.Text = lb_welcome.Text.Replace("%username%", $"{Helpers.CurrentUser!.Username}"); // Reemplaza el nombre de usuario en el texto de bienvenida
            }
            else
            {
                administracionToolStripMenuItem.Enabled = false; // Desactiva el men� de administraci�n
                btn_products.Visible = false; // Oculta el bot�n de productos
                btn_promo.Visible = false; // Oculta el bot�n de promoci�n
                lb_welcome.ForeColor = Color.Green; // Cambia el color del texto a verde
                lb_welcome.Text = lb_welcome.Text.Replace("%username%", $"{Helpers.CurrentUser!.Username}"); // Reemplaza el nombre de usuario en el texto de bienvenida
            }
        }

        // Evento que se dispara cuando se hace clic en la opci�n de agregar usuario del men�
        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddUser adduser = new(); // Crea una nueva instancia del formulario AddUser
            adduser.ShowDialog(); // Muestra el formulario de agregar usuario como un cuadro de di�logo
        }

        // Evento que se dispara cuando se hace clic en la opci�n de eliminar usuario del men�
        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DelUser delUser = new(); // Crea una nueva instancia del formulario DelUser
            delUser.ShowDialog(); // Muestra el formulario de eliminar usuario como un cuadro de di�logo
        }

        // Evento que se dispara cuando se hace clic en la opci�n de borrar configuraci�n del men�
        private void borrarConfiguracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("�Deseas borrar los datos de confguraci�n de la base de datos?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                File.Delete("ConnectionData.cnf"); // Elimina el archivo de configuraci�n de la base de datos
                MessageBox.Show("Configuraci�n eliminada se cerrar� el programa."); // Muestra un mensaje de confirmaci�n
                Environment.Exit(0); // Cierra la aplicaci�n
            }
        }

        // Evento que se dispara cuando se hace clic en el label6 (vac�o, sin implementar)
        private void label6_Click(object sender, EventArgs e)
        {

        }

        // Evento que se dispara cuando se presiona el bot�n del rat�n en el formulario
        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            _move = true; // Activa el movimiento del formulario
            mx = e.X; my = e.Y; // Almacena las coordenadas del rat�n
        }

        // Evento que se dispara cuando se mueve el rat�n sobre el formulario
        private void Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (_move) SetDesktopLocation(MousePosition.X - mx, MousePosition.Y - my); // Mueve el formulario con el rat�n
        }

        // Evento que se dispara cuando se suelta el bot�n del rat�n en el formulario
        private void Main_MouseUp(object sender, MouseEventArgs e)
        {
            _move = false; // Desactiva el movimiento del formulario
        }

        // Evento que se dispara cuando se hace clic en el bot�n de salir
        private void btn_exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0); // Cierra la aplicaci�n
        }

        // Evento que se dispara cuando se hace clic en el label4 (minimizar ventana)
        private void label4_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized; // Minimiza la ventana
        }

        // Evento que se dispara cuando se hace clic en el bot�n de cerrar sesi�n
        private void btn_logout_Click(object sender, EventArgs e)
        {
            Helpers.login = false; // Cambia el estado de login a falso
            Close(); // Cierra el formulario
        }
    }
}
