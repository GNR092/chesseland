﻿namespace Chesseland
{
    partial class Ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventas));
            datag_ventas = new DataGridView();
            c_Producto = new DataGridViewTextBoxColumn();
            c_precio = new DataGridViewTextBoxColumn();
            c_cantidad = new DataGridViewTextBoxColumn();
            c_subtotal = new DataGridViewTextBoxColumn();
            lb_total = new Label();
            txt_total = new TextBox();
            pictureBox3 = new PictureBox();
            label14 = new Label();
            btn_search = new Button();
            label13 = new Label();
            btn_exit = new Button();
            pictureBox4 = new PictureBox();
            label12 = new Label();
            pictureBox1 = new PictureBox();
            pictureBox5 = new PictureBox();
            btn_addp = new Button();
            button1 = new Button();
            btn_addv = new Button();
            ((System.ComponentModel.ISupportInitialize)datag_ventas).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).BeginInit();
            SuspendLayout();
            // 
            // datag_ventas
            // 
            datag_ventas.BackgroundColor = Color.FromArgb(255, 224, 192);
            datag_ventas.ColumnHeadersHeight = 29;
            datag_ventas.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            datag_ventas.Columns.AddRange(new DataGridViewColumn[] { c_Producto, c_precio, c_cantidad, c_subtotal });
            datag_ventas.GridColor = Color.FromArgb(255, 128, 0);
            datag_ventas.Location = new Point(37, 111);
            datag_ventas.Margin = new Padding(3, 2, 3, 2);
            datag_ventas.Name = "datag_ventas";
            datag_ventas.ReadOnly = true;
            datag_ventas.RowHeadersWidth = 51;
            datag_ventas.RowTemplate.Height = 29;
            datag_ventas.Size = new Size(643, 196);
            datag_ventas.TabIndex = 1;
            // 
            // c_Producto
            // 
            c_Producto.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_Producto.HeaderText = "Productos/Promo";
            c_Producto.MinimumWidth = 6;
            c_Producto.Name = "c_Producto";
            c_Producto.ReadOnly = true;
            c_Producto.Resizable = DataGridViewTriState.False;
            // 
            // c_precio
            // 
            c_precio.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_precio.HeaderText = "Precio";
            c_precio.MinimumWidth = 6;
            c_precio.Name = "c_precio";
            c_precio.ReadOnly = true;
            // 
            // c_cantidad
            // 
            c_cantidad.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_cantidad.HeaderText = "Cantidad";
            c_cantidad.MinimumWidth = 6;
            c_cantidad.Name = "c_cantidad";
            c_cantidad.ReadOnly = true;
            // 
            // c_subtotal
            // 
            c_subtotal.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            c_subtotal.HeaderText = "Subtotal";
            c_subtotal.MinimumWidth = 6;
            c_subtotal.Name = "c_subtotal";
            c_subtotal.ReadOnly = true;
            // 
            // lb_total
            // 
            lb_total.AutoSize = true;
            lb_total.Font = new Font("Roboto", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            lb_total.Location = new Point(490, 318);
            lb_total.Name = "lb_total";
            lb_total.Size = new Size(42, 17);
            lb_total.TabIndex = 2;
            lb_total.Text = "Total:";
            // 
            // txt_total
            // 
            txt_total.Location = new Point(554, 312);
            txt_total.Name = "txt_total";
            txt_total.ReadOnly = true;
            txt_total.Size = new Size(126, 23);
            txt_total.TabIndex = 30;
            txt_total.Text = "$";
            // 
            // pictureBox3
            // 
            pictureBox3.Image = Properties.Resources.descargar;
            pictureBox3.Location = new Point(6, 27);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(185, 50);
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.TabIndex = 35;
            pictureBox3.TabStop = false;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Font = new Font("Roboto", 22.2F, FontStyle.Bold, GraphicsUnit.Point);
            label14.Location = new Point(320, 40);
            label14.Name = "label14";
            label14.Size = new Size(130, 37);
            label14.TabIndex = 36;
            label14.Text = "VENTAS";
            // 
            // btn_search
            // 
            btn_search.BackColor = Color.Goldenrod;
            btn_search.FlatStyle = FlatStyle.Flat;
            btn_search.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_search.Location = new Point(570, 356);
            btn_search.Margin = new Padding(3, 2, 3, 2);
            btn_search.Name = "btn_search";
            btn_search.Size = new Size(110, 22);
            btn_search.TabIndex = 37;
            btn_search.Text = "COBRAR";
            btn_search.UseVisualStyleBackColor = false;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.BackColor = Color.White;
            label13.Font = new Font("Roboto", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            label13.Location = new Point(629, -5);
            label13.Name = "label13";
            label13.Size = new Size(18, 27);
            label13.TabIndex = 42;
            label13.Text = "-";
            // 
            // btn_exit
            // 
            btn_exit.BackColor = Color.Crimson;
            btn_exit.FlatStyle = FlatStyle.Flat;
            btn_exit.Location = new Point(647, 0);
            btn_exit.Margin = new Padding(3, 2, 3, 2);
            btn_exit.Name = "btn_exit";
            btn_exit.Size = new Size(21, 20);
            btn_exit.TabIndex = 41;
            btn_exit.Text = "X";
            btn_exit.UseVisualStyleBackColor = false;
            btn_exit.Click += btn_exit_Click;
            // 
            // pictureBox4
            // 
            pictureBox4.Image = (Image)resources.GetObject("pictureBox4.Image");
            pictureBox4.Location = new Point(672, 27);
            pictureBox4.Name = "pictureBox4";
            pictureBox4.Size = new Size(27, 29);
            pictureBox4.TabIndex = 40;
            pictureBox4.TabStop = false;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(597, 34);
            label12.Name = "label12";
            label12.Size = new Size(69, 14);
            label12.TabIndex = 39;
            label12.Text = "Chesseland";
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox1.Location = new Point(6, -2);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(622, 20);
            pictureBox1.TabIndex = 38;
            pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            pictureBox5.Image = Properties.Resources.diseno_banner_corporativo_estilo_onda_elegante_color_amarillo_1055_13746;
            pictureBox5.Location = new Point(628, -2);
            pictureBox5.Name = "pictureBox5";
            pictureBox5.Size = new Size(89, 20);
            pictureBox5.TabIndex = 43;
            pictureBox5.TabStop = false;
            // 
            // btn_addp
            // 
            btn_addp.BackColor = Color.Goldenrod;
            btn_addp.FlatStyle = FlatStyle.Flat;
            btn_addp.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_addp.Location = new Point(37, 313);
            btn_addp.Margin = new Padding(3, 2, 3, 2);
            btn_addp.Name = "btn_addp";
            btn_addp.Size = new Size(154, 22);
            btn_addp.TabIndex = 44;
            btn_addp.Text = "AGREGAR PRODUCTO";
            btn_addp.UseVisualStyleBackColor = false;
            btn_addp.Click += btn_add_Click;
            // 
            // button1
            // 
            button1.BackColor = Color.Goldenrod;
            button1.FlatStyle = FlatStyle.Flat;
            button1.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            button1.Location = new Point(338, 356);
            button1.Margin = new Padding(3, 2, 3, 2);
            button1.Name = "button1";
            button1.Size = new Size(110, 22);
            button1.TabIndex = 45;
            button1.Text = "ACTUALIZAR";
            button1.UseVisualStyleBackColor = false;
            // 
            // btn_addv
            // 
            btn_addv.BackColor = Color.Goldenrod;
            btn_addv.FlatStyle = FlatStyle.Flat;
            btn_addv.Font = new Font("Roboto", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btn_addv.Location = new Point(454, 356);
            btn_addv.Margin = new Padding(3, 2, 3, 2);
            btn_addv.Name = "btn_addv";
            btn_addv.Size = new Size(110, 22);
            btn_addv.TabIndex = 46;
            btn_addv.Text = "AGREGAR";
            btn_addv.UseVisualStyleBackColor = false;
            btn_addv.Click += btn_addv_Click;
            // 
            // Ventas
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(716, 404);
            Controls.Add(btn_addv);
            Controls.Add(button1);
            Controls.Add(btn_addp);
            Controls.Add(label13);
            Controls.Add(btn_exit);
            Controls.Add(pictureBox5);
            Controls.Add(pictureBox4);
            Controls.Add(label12);
            Controls.Add(pictureBox1);
            Controls.Add(btn_search);
            Controls.Add(label14);
            Controls.Add(pictureBox3);
            Controls.Add(txt_total);
            Controls.Add(lb_total);
            Controls.Add(datag_ventas);
            FormBorderStyle = FormBorderStyle.None;
            Margin = new Padding(3, 2, 3, 2);
            Name = "Ventas";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Ventas";
            MouseDown += Ventas_MouseDown;
            MouseMove += Ventas_MouseMove;
            MouseUp += Ventas_MouseUp;
            ((System.ComponentModel.ISupportInitialize)datag_ventas).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DataGridView datag_ventas;
        private Label lb_total;
#pragma warning disable CS0169 // El campo 'Ventas.pictureBox2' nunca se usa
        private PictureBox pictureBox2;
#pragma warning restore CS0169 // El campo 'Ventas.pictureBox2' nunca se usa
        private TextBox txt_total;
        private PictureBox pictureBox3;
        private Label label14;
        private Button btn_search;
        private Label label13;
        private Button btn_exit;
        private PictureBox pictureBox4;
        private Label label12;
        private PictureBox pictureBox1;
        private PictureBox pictureBox5;
        private Button btn_addp;
        private DataGridViewTextBoxColumn c_Producto;
        private DataGridViewTextBoxColumn c_precio;
        private DataGridViewTextBoxColumn c_cantidad;
        private DataGridViewTextBoxColumn c_subtotal;
        private Button button1;
        private Button btn_addv;
    }
}