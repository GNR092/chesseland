﻿using Chesseland.db; // Importa el namespace Chesseland.db
using Chesseland.forms; // Importa el namespace Chesseland.forms

namespace Chesseland
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Ventas : Form
    {
        private bool _move = false; // Variable para controlar el movimiento del formulario
        private int mx, my; // Variables para almacenar las coordenadas del ratón
        /// <summary>
        /// Variable estática para almacenar un producto temporal
        /// </summary>
        public static Producto? tmpprodct;
        /// <summary>
        /// Variable estática para contar algo (no se usa en el código)
        /// </summary>
        public static int count; 
        private List<int> ventas; // Lista para almacenar los valores de ventas

        /// <summary>
        /// 
        /// </summary>
        public Ventas()
        {
            InitializeComponent(); // Inicializa los componentes del formulario
            ventas = new(); // Inicializa la lista de ventas
        }

        // Evento que se dispara cuando se presiona el botón del ratón en el formulario
        private void Ventas_MouseDown(object sender, MouseEventArgs e)
        {
            _move = true; // Activa el movimiento del formulario
            mx = e.X; my = e.Y; // Almacena las coordenadas del ratón
        }

        // Evento que se dispara cuando se mueve el ratón sobre el formulario
        private void Ventas_MouseMove(object sender, MouseEventArgs e)
        {
            if (_move) SetDesktopLocation(MousePosition.X - mx, MousePosition.Y - my); // Mueve el formulario con el ratón
        }

        // Evento que se dispara cuando se suelta el botón del ratón en el formulario
        private void Ventas_MouseUp(object sender, MouseEventArgs e)
        {
            _move = false; // Desactiva el movimiento del formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de salir
        private void btn_exit_Click(object sender, EventArgs e)
        {
            Close(); // Cierra el formulario
        }

        // Evento que se dispara cuando se hace clic en el botón de agregar
        private void btn_add_Click(object sender, EventArgs e)
        {
            var addp = new ViewProducts(); // Crea una nueva instancia del formulario ViewProducts
            addp.ShowDialog(); // Muestra el formulario de productos y espera a que se cierre
            if (tmpprodct != null) // Si se seleccionó un producto temporal
            {
                var pd = tmpprodct; // Asigna el producto temporal a una variable local
                tmpprodct = null; // Resetea el producto temporal
                int index = datag_ventas.Rows.Add(); // Agrega una nueva fila al DataGridView
                datag_ventas.Rows[index].Cells[0].Value = pd.Nombre; // Asigna el nombre del producto a la celda
                datag_ventas.Rows[index].Cells[1].Value = pd.Precio; // Asigna el precio del producto a la celda
                datag_ventas.Rows[index].Cells[2].Value = pd.Cantidad; // Asigna la cantidad del producto a la celda
                datag_ventas.Rows[index].Cells[3].Value = (pd.Cantidad * pd.Precio); // Calcula y asigna el total
                ventas.Add((pd.Cantidad * pd.Precio)); // Agrega el total a la lista de ventas
            }
            int count = 0; // Inicializa un contador
            foreach (int pc in ventas) // Recorre la lista de ventas
            {
                count += pc; // Suma los valores de ventas al contador
            }
            txt_total.Text = $"${count}"; // Muestra el total en el TextBox

        }

        // Método para obtener los valores de una celda específica del DataGridView
        private object GetDataValues(int index)
        {
            DataGridViewCell a = datag_ventas.CurrentCell; // Obtiene la celda actual del DataGridView
            return datag_ventas.Rows[a.RowIndex].Cells[index].Value; // Retorna el valor de la celda especificada
        }

        // Evento que se dispara cuando se hace clic en el botón de agregar venta (aún no implementado)
        private void btn_addv_Click(object sender, EventArgs e)
        {

        }
    }
}
