﻿namespace Chesseland
{
    /// <summary>
    /// Define los diferentes roles que pueden existir en el sistema.
    /// </summary>
    public enum Roles
    {
        /// <summary>
        /// Rol no especificado.
        /// </summary>
        None = 0,

        /// <summary>
        /// Rol de administrador.
        /// </summary>
        Admin = 1,

        /// <summary>
        /// Rol de cajero.
        /// </summary>
        Cajero = 2,

        /// <summary>
        /// Rol de mesero.
        /// </summary>
        Mesero = 3,

        /// <summary>
        /// Rol de superadministrador con privilegios especiales.
        /// </summary>
        Superadmin = 99
    }
}
