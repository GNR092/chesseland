﻿using MySql.Data.MySqlClient;
using Mysqlext;
using System.Data;

namespace Chesseland.db
{
    internal class SaleManager
    {
        private IDbConnection database;
        public SaleManager(IDbConnection db)
        {
            database = db;
            var table = new SqlTable("ventas",
                new SqlColumn("idventa", MySqlDbType.Int32) { Primary = true, AutoIncrement = true },
                new SqlColumn("nummesa", MySqlDbType.Int32) { NotNull = true},
                new SqlColumn("idproducto", MySqlDbType.Int32),
                new SqlColumn("idpromo", MySqlDbType.Int32),
                new SqlColumn("total", MySqlDbType.Int32) { NotNull = true}
                );
            var creator = new SqlTableCreator(db, new MysqlQueryCreator());
            creator.EnsureTableStructure(table);
        }
    }
}
