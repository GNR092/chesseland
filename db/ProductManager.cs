﻿using System.Data;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using Mysqlext;
using Mysqlext.Extensions;

namespace Chesseland.db
{
    /// <summary>
    /// Clase encargada de gestionar los productos en la base de datos.
    /// </summary>
    internal class ProductManager
    {
        private IDbConnection database;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="ProductManager"/>.
        /// </summary>
        /// <param name="db">Conexión a la base de datos.</param>
        public ProductManager(IDbConnection db)
        {
            database = db;
            var table = new SqlTable("productos",
                new SqlColumn("Idproducto", MySqlDbType.Int32) { Primary = true, AutoIncrement = true },
                new SqlColumn("nombre", MySqlDbType.VarChar, 100),
                new SqlColumn("precio", MySqlDbType.Int32),
                new SqlColumn("ingredientes", MySqlDbType.VarChar, 200)
            );
            var creator = new SqlTableCreator(db, new MysqlQueryCreator());
            creator.EnsureTableStructure(table);
        }

        /// <summary>
        /// Añade un nuevo producto a la base de datos.
        /// </summary>
        /// <param name="product">Producto a añadir.</param>
        public void AddProduct(Producto product)
        {
            int ret;
            try
            {
                ret = database.Query("INSERT INTO productos (nombre, precio, ingredientes) VALUES (@0, @1, @2);", product.Nombre!, product.Precio, product.Ingredientes!);
            }
            catch (Exception ex)
            {
                if (Regex.IsMatch(ex.Message, "Nombre.*not unique"))
                {
                    throw new ProductExistsException(product.Nombre!);
                }
                throw new ProductManagerException("AddProduct SQL returned an error (" + ex.Message + ")", ex);
            }
            if (ret < 1)
            {
                throw new ProductExistsException(product.Nombre!);
            }
        }

        /// <summary>
        /// Actualiza un producto existente en la base de datos.
        /// </summary>
        /// <param name="p">Producto a actualizar.</param>
        public void UpdateProduct(Producto p)
        {
            try
            {
                if (database.Query("UPDATE productos SET nombre = @0, precio = @1, ingredientes = @2 WHERE Idproducto = @3;", p.Nombre!, p.Precio, p.Ingredientes!, p.Idproducto) == 0)
                {
                    throw new ProductNotExistException(p.Nombre!);
                }
            }
            catch (Exception ex)
            {
                throw new ProductManagerException("UpdateProduct SQL returned an error", ex);
            }
        }

        /// <summary>
        /// Busca productos por nombre.
        /// </summary>
        /// <param name="pname">Nombre del producto a buscar.</param>
        /// <returns>Lista de productos que coinciden con el nombre.</returns>
        public List<Producto> SearchProduct(string pname)
        {
            try
            {
                List<Producto> productos = new();
                using (var reader = database.QueryReader($"SELECT * FROM productos WHERE nombre LIKE '{pname}%'"))
                {
                    while (reader.Read())
                    {
                        productos.Add(LoadProductFromResult(new Producto(), reader));
                    }
                    return productos;
                }
            }
            catch (Exception ex)
            {
                throw new ProductManagerException("SearchProduct SQL returned an error", ex);
            }
        }

        /// <summary>
        /// Obtiene todos los productos de la base de datos.
        /// </summary>
        /// <returns>Lista de todos los productos.</returns>
        public List<Producto> GetProducts()
        {
            try
            {
                List<Producto> products = new();
                using (var reader = database.QueryReader("SELECT * FROM productos"))
                {
                    while (reader.Read())
                    {
                        products.Add(LoadProductFromResult(new Producto(), reader));
                    }
                    return products;
                }
            }
            catch (Exception ex)
            {
                throw new ProductManagerException("GetProducts SQL returned an error (" + ex.Message + ")", ex);
            }
        }

        /// <summary>
        /// Carga un producto a partir de un resultado de consulta.
        /// </summary>
        /// <param name="product">Producto a cargar.</param>
        /// <param name="result">Resultado de la consulta.</param>
        /// <returns>Producto cargado.</returns>
        private Producto LoadProductFromResult(Producto product, QueryResult result)
        {
            product.Idproducto = result.Get<int>("Idproducto");
            product.Nombre = result.Get<string>("nombre")!;
            product.Precio = result.Get<int>("precio");
            product.Ingredientes = result.Get<string>("ingredientes")!;
            return product;
        }
    }

    /// <summary>
    /// Clase que representa un producto.
    /// </summary>
    public class Producto
    {
        /// <summary>
        /// Idproducto
        /// </summary>
        public int Idproducto { get; set; }
        /// <summary>
        /// Nombre
        /// </summary>
        public string? Nombre { get; set; }
        /// <summary>
        /// Precio
        /// </summary>
        public int Precio { get; set; }
        /// <summary>
        /// Ingredientes
        /// </summary>
        public string? Ingredientes { get; set; }
        /// <summary>
        /// Cantidad
        /// </summary>
        public int Cantidad { get; set; }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Producto"/>.
        /// </summary>
        public Producto()
        {
            Idproducto = 0;
            Nombre = "";
            Precio = 0;
            Ingredientes = "";
            Cantidad = 0;
        }
    }

    /// <summary>
    /// Excepción lanzada cuando un producto no existe y una consulta falla como resultado de ello.
    /// </summary>
    [Serializable]
    public class ProductNotExistException : ProductManagerException
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="ProductNotExistException"/> con el nombre del producto en el mensaje.
        /// </summary>
        /// <param name="name">El nombre del producto que no existe.</param>
        public ProductNotExistException(string name)
            : base("Producto '" + name + "' no existe")
        {
        }
    }

    /// <summary>
    /// Excepción lanzada cuando un producto ya existe al intentar crear uno nuevo.
    /// </summary>
    [Serializable]
    public class ProductExistsException : ProductManagerException
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="ProductExistsException"/>.
        /// </summary>
        /// <param name="name">El nombre del producto que ya existe.</param>
        public ProductExistsException(string name)
            : base("Producto '" + name + "' ya existe")
        {
        }
    }

    /// <summary>
    /// Excepción generada por el gestor de productos.
    /// </summary>
    [Serializable]
    public class ProductManagerException : Exception
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="ProductManagerException"/>.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        public ProductManagerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Crea un nuevo objeto <see cref="ProductManagerException"/> con una excepción interna.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        /// <param name="inner">La excepción interna.</param>
        public ProductManagerException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
