﻿using System.Data;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using Mysqlext;
using Mysqlext.Extensions;

namespace Chesseland.db
{
    /// <summary>
    /// Clase encargada de gestionar las promociones en la base de datos.
    /// </summary>
    internal class PromoManager
    {
        private IDbConnection database;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="PromoManager"/>.
        /// </summary>
        /// <param name="db">Conexión a la base de datos.</param>
        public PromoManager(IDbConnection db)
        {
            database = db;
            var table = new SqlTable("promos",
                new SqlColumn("idpromo", MySqlDbType.Int32) { Primary = true, AutoIncrement = true },
                new SqlColumn("idproducto", MySqlDbType.Int32),
                new SqlColumn("nompromo", MySqlDbType.VarChar, 100),
                new SqlColumn("ingredientes", MySqlDbType.VarChar, 200),
                new SqlColumn("extra", MySqlDbType.VarChar, 200),
                new SqlColumn("preciopromo", MySqlDbType.Int32)
            );
            var creator = new SqlTableCreator(db, new MysqlQueryCreator());
            creator.EnsureTableStructure(table);
        }

        /// <summary>
        /// Añade una nueva promoción a la base de datos.
        /// </summary>
        /// <param name="p">Promoción a añadir.</param>
        public void AddPromo(Promo p)
        {
            int ret;
            try
            {
                ret = database.Query("INSERT INTO promos (idproducto, nompromo, ingredientes, extra, preciopromo) VALUES (@0, @1, @2, @3, @4);", p.Idproduct, p.Nombpromo, p.Ingredientes, p.Extra, p.Preciopromo);
            }
            catch (Exception ex)
            {
                if (Regex.IsMatch(ex.Message, "Nombpromo.*not unique"))
                {
                    throw new PromoExistException(p.Nombpromo!);
                }
                throw new PromoManagerException("AddPromo SQL returned an error (" + ex.Message + ")", ex);
            }
            if (ret < 1)
            {
                throw new PromoExistException(p.Nombpromo!);
            }
        }

        /// <summary>
        /// Actualiza una promoción existente en la base de datos.
        /// </summary>
        /// <param name="p">Promoción a actualizar.</param>
        public void UpdatePromo(Promo p)
        {
            try
            {
                if (database.Query("UPDATE promos SET idproducto = @0, nompromo = @1, ingredientes = @2, extra = @3, preciopromo = @4 WHERE idpromo = @5;", p.Idproduct, p.Nombpromo, p.Ingredientes, p.Extra, p.Preciopromo, p.Idpromo) == 0)
                {
                    throw new PromoNotExistException(p.Nombpromo!);
                }
            }
            catch (Exception ex)
            {
                throw new PromoManagerException("UpdatePromo SQL returned an error", ex);
            }
        }

        /// <summary>
        /// Obtiene todas las promociones de la base de datos.
        /// </summary>
        /// <returns>Lista de todas las promociones.</returns>
        public List<Promo> GetPromo()
        {
            try
            {
                List<Promo> promos = new();
                using (var reader = database.QueryReader("SELECT * FROM promos"))
                {
                    while (reader.Read())
                    {
                        promos.Add(LoadPromoFromResult(new Promo(), reader));
                    }
                    return promos;
                }
            }
            catch (Exception ex)
            {
                throw new PromoManagerException("GetPromo SQL returned an error (" + ex.Message + ")", ex);
            }
        }

        /// <summary>
        /// Carga una promoción a partir de un resultado de consulta.
        /// </summary>
        /// <param name="p">Promoción a cargar.</param>
        /// <param name="result">Resultado de la consulta.</param>
        /// <returns>Promoción cargada.</returns>
        private Promo LoadPromoFromResult(Promo p, QueryResult result)
        {
            p.Idpromo = result.Get<int>("idpromo");
            p.Idproduct = result.Get<int>("idproducto");
            p.Nombpromo = result.Get<string>("nompromo")!;
            p.Ingredientes = result.Get<string>("ingredientes")!;
            p.Extra = result.Get<string>("extra")!;
            p.Preciopromo = result.Get<int>("preciopromo");
            return p;
        }
    }

    /// <summary>
    /// Clase que representa una promoción.
    /// </summary>
    public class Promo
    {
        /// <summary>
        /// Obtiene o establece el ID de la promoción.
        /// </summary>
        public int Idpromo { get; set; }

        /// <summary>
        /// Obtiene o establece el ID del producto asociado a la promoción.
        /// </summary>
        public int Idproduct { get; set; }

        /// <summary>
        /// Obtiene o establece el nombre de la promoción.
        /// </summary>
        public string Nombpromo { get; set; }

        /// <summary>
        /// Obtiene o establece los ingredientes adicionales incluidos en la promoción.
        /// </summary>
        public string Ingredientes { get; set; }

        /// <summary>
        /// Obtiene o establece información adicional o extra de la promoción.
        /// </summary>
        public string Extra { get; set; }

        /// <summary>
        /// Obtiene o establece el precio de la promoción.
        /// </summary>
        public int Preciopromo { get; set; }


        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Promo"/>.
        /// </summary>
        public Promo()
        {
            Idpromo = 0;
            Idproduct = 0;
            Nombpromo = "";
            Ingredientes = "";
            Extra = "";
            Preciopromo = 0;
        }
    }

    /// <summary>
    /// Excepción lanzada cuando una promoción no existe y una consulta falla como resultado de ello.
    /// </summary>
    [Serializable]
    public class PromoNotExistException : PromoManagerException
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="PromoNotExistException"/> con el nombre de la promoción en el mensaje.
        /// </summary>
        /// <param name="name">El nombre de la promoción que no existe.</param>
        public PromoNotExistException(string name) : base("Promo '" + name + "' no existe") { }
    }

    /// <summary>
    /// Excepción lanzada cuando una promoción ya existe al intentar crear una nueva.
    /// </summary>
    [Serializable]
    public class PromoExistException : PromoManagerException
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="PromoExistException"/>.
        /// </summary>
        /// <param name="name">El nombre de la promoción que ya existe.</param>
        public PromoExistException(string name) : base("Promo '" + name + "' ya existe") { }
    }

    /// <summary>
    /// Excepción general para errores del gestor de promociones.
    /// </summary>
    [Serializable]
    public class PromoManagerException : Exception
    {
        /// <summary>
        /// Crea un nuevo objeto <see cref="PromoManagerException"/>.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        public PromoManagerException(string message) : base(message) { }

        /// <summary>
        /// Crea un nuevo objeto <see cref="PromoManagerException"/> con una excepción interna.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        /// <param name="inner">La excepción interna.</param>
        public PromoManagerException(string message, Exception inner) : base(message, inner) { }
    }
}
