﻿using System.Text;
using System.Security.Cryptography;

namespace Chesseland.db
{
    /// <summary>
    /// Provee y manipula el archivo de configuración.
    /// </summary>
    public class Config
    {
        /// <summary>
        /// Nombre de la base de datos.
        /// </summary>
        public string? Database { get; set; }

        /// <summary>
        /// Nombre de usuario para la conexión a la base de datos.
        /// </summary>
        public string? Username { get; set; }

        /// <summary>
        /// Contraseña para la conexión a la base de datos.
        /// </summary>
        public string? Password { get; set; }

        /// <summary>
        /// Host de la base de datos.
        /// </summary>
        public string? Host { get; set; }

        /// <summary>
        /// Puerto para la conexión a la base de datos.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Llave de encriptación.
        /// </summary>
        private readonly byte[] key = Encoding.UTF8.GetBytes("myEncryptionKeyCh3ss314n"); // tamaño de 24

        /// <summary>
        /// Vector de inicialización para la encriptación.
        /// </summary>
        private readonly byte[] iv = Encoding.UTF8.GetBytes("Ch3ss314"); // tamaño de 8

        /// <summary>
        /// Crea y encripta todos los parámetros del archivo de configuración.
        /// </summary>
        public void EncriptConnectionData()
        {
            using TripleDES tripleDes = TripleDES.Create();
            tripleDes.Key = key;
            tripleDes.IV = iv;
            using ICryptoTransform encryptor = tripleDes.CreateEncryptor(tripleDes.Key, tripleDes.IV);
            using FileStream fsEncrypt = new("ConnectionData.cnf", FileMode.Create);
            using CryptoStream csEncrypt = new(fsEncrypt, encryptor, CryptoStreamMode.Write);
            var enc = Encoding.UTF8.GetBytes($"{Database},{Username},{Password},{Host},{Port}");
            csEncrypt.Write(enc, 0, enc.Length);
        }

        /// <summary>
        /// Lee el archivo de configuración y lo desencripta regresando los valores de conexión.
        /// </summary>
        public void DesencryptConnectionData()
        {
            string[] configValues;
            using (TripleDES tripleDes = TripleDES.Create())
            {
                tripleDes.Key = key;
                tripleDes.IV = iv;

                ICryptoTransform decryptor = tripleDes.CreateDecryptor(tripleDes.Key, tripleDes.IV);

                using FileStream fsDecrypt = new("ConnectionData.cnf", FileMode.Open);
                using CryptoStream csDecrypt = new(fsDecrypt, decryptor, CryptoStreamMode.Read);
                using StreamReader srDecrypt = new(csDecrypt);
                string decryptedText = srDecrypt.ReadToEnd();
                configValues = decryptedText.Split(',');
            }
            Database = configValues[0];
            Username = configValues[1];
            Password = configValues[2];
            Host = configValues[3];
            Port = int.Parse(configValues[4]);
        }
    }
}
