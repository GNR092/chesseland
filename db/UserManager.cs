﻿using System.Data;
using System.Text.RegularExpressions;
using BCrypt.Net;
using MySql.Data.MySqlClient;
using Mysqlext;
using Mysqlext.Extensions;

namespace Chesseland.db
{
    /// <summary>
    /// Manipula la tabla de Usuarios así como agregar, eliminar y actualizar datos.
    /// </summary>
    internal class UserManager
    {
        private IDbConnection database;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="UserManager"/>.
        /// </summary>
        /// <param name="db">Conexión a la base de datos.</param>
        public UserManager(IDbConnection db)
        {
            database = db;
            var table = new SqlTable("Users",
                new SqlColumn("IdUser", MySqlDbType.Int32) { Primary = true, AutoIncrement = true },
                new SqlColumn("Username", MySqlDbType.VarChar, 32) { Unique = true },
                new SqlColumn("Rol", MySqlDbType.Int32, 2),
                new SqlColumn("Password", MySqlDbType.VarChar, 128),
                new SqlColumn("Email", MySqlDbType.VarChar, 128)
            );
            var creator = new SqlTableCreator(db, new MysqlQueryCreator());
            creator.EnsureTableStructure(table);
        }

        /// <summary>
        /// Actualiza un usuario.
        /// </summary>
        /// <param name="user">Usuario a actualizar.</param>
        /// <exception cref="UserNotExistException">Lanzada cuando el usuario no existe.</exception>
        /// <exception cref="UserManagerException">Lanzada cuando hay un error en la consulta SQL.</exception>
        public void UpdateUser(User user)
        {
            try
            {
                string query;
                if (!string.IsNullOrEmpty(user.Password))
                {
                    query = "UPDATE Users SET Username = @0, Rol = @1, Password = @2, Email = @3 WHERE IdUser = @4;";
                }
                else
                {
                    query = "UPDATE Users SET Username = @0, Rol = @1, Email = @2 WHERE IdUser = @3;";
                }
                if (database.Query(query, user.Username, user.Rol, user.Password, user.Email, user.IdUser) == 0)
                {
                    throw new UserNotExistException(user.Username);
                }
            }
            catch (Exception ex)
            {
                throw new UserManagerException("UpdateUser SQL returned an error", ex);
            }
        }

        /// <summary>
        /// Agrega un nuevo usuario.
        /// </summary>
        /// <param name="user">Usuario a agregar.</param>
        /// <exception cref="UserExistsException">Lanzada cuando el usuario ya existe.</exception>
        /// <exception cref="UserManagerException">Lanzada cuando hay un error en la consulta SQL.</exception>
        public void AddUser(User user)
        {
            int ret;
            try
            {
                ret = database.Query("INSERT INTO Users (Username, Rol, Password, Email) VALUES (@0, @1, @2, @3);", user.Username, user.Rol, user.Password, user.Email);
            }
            catch (Exception ex)
            {
                if (Regex.IsMatch(ex.Message, "Username.*not unique"))
                {
                    throw new UserExistsException(user.Username);
                }
                throw new UserManagerException("AddUser SQL returned an error (" + ex.Message + ")", ex);
            }
            if (ret < 1)
            {
                throw new UserExistsException(user.Username);
            }
        }

        /// <summary>
        /// Elimina un usuario ya existente.
        /// </summary>
        /// <param name="user">Usuario a eliminar.</param>
        /// <exception cref="UserNotExistException">Lanzada cuando el usuario no existe.</exception>
        /// <exception cref="UserManagerException">Lanzada cuando hay un error en la consulta SQL.</exception>
        public void RemoveUser(User user)
        {
            try
            {
                var tempuser = GetUser(user);
                int affected = database.Query("DELETE FROM Users WHERE Username = @0", user.Username);
                if (affected < 1)
                {
                    throw new UserNotExistException(user.Username);
                }
            }
            catch (Exception ex)
            {
                throw new UserManagerException("RemoveUser SQL returned an error", ex);
            }
        }

        /// <summary>
        /// Obtiene un usuario por su nombre.
        /// </summary>
        /// <param name="name">Nombre del usuario.</param>
        /// <returns>Un usuario si existe, de lo contrario null.</returns>
        public User? GetUserByName(string name)
        {
            try
            {
                return GetUser(new User { Username = name });
            }
            catch (UserManagerException)
            {
                return null;
            }
        }

        /// <summary>
        /// Obtiene un usuario por su información.
        /// </summary>
        /// <param name="user">Información del usuario.</param>
        /// <returns>El usuario encontrado.</returns>
        /// <exception cref="UserManagerException">Lanzada cuando hay un error en la consulta SQL.</exception>
        /// <exception cref="UserNotExistException">Lanzada cuando el usuario no existe.</exception>
        private User GetUser(User user)
        {
            bool multiple = false;
            string query;
            object arg;

            if (user.IdUser != 0)
            {
                query = "SELECT * FROM Users WHERE IdUser = @0";
                arg = user.IdUser;
            }
            else
            {
                query = "SELECT * FROM Users WHERE Username = @0";
                arg = user.Username;
            }
            try
            {
                using (var result = database.QueryReader(query, arg))
                {
                    if (result.Read())
                    {
                        user = LoadUserFromResult(user, result);
                        if (result.Read())
                        {
                            multiple = true;
                        }
                        else
                        {
                            return user;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserManagerException("Error (" + ex.Message + ")", ex);
            }
            if (multiple)
            {
                throw new UserManagerException(string.Format("Múltiples usuarios encontrados para {0}", arg));
            }
            throw new UserNotExistException(user.Username);
        }

        /// <summary>
        /// Obtiene todos los usuarios.
        /// </summary>
        /// <returns>Una lista de usuarios encontrados.</returns>
        /// <exception cref="UserManagerException">Lanzada cuando hay un error en la consulta SQL.</exception>
        public List<User> GetUsers()
        {
            try
            {
                List<User> users = new();
                using (var reader = database.QueryReader("SELECT * FROM Users"))
                {
                    while (reader.Read())
                    {
                        users.Add(LoadUserFromResult(new User(), reader));
                    }
                    return users;
                }
            }
            catch (Exception ex)
            {
                throw new UserManagerException("GetUsers SQL returned an error (" + ex.Message + ")", ex);
            }
        }

        /// <summary>
        /// Carga la información de un usuario a partir de un resultado de consulta.
        /// </summary>
        /// <param name="user">Usuario a cargar.</param>
        /// <param name="result">Resultado de la consulta.</param>
        /// <returns>La información del usuario.</returns>
        private User LoadUserFromResult(User user, QueryResult result)
        {
            user.IdUser = result.Get<int>("IdUser");
            user.Username = result.Get<string>("Username")!;
            user.Rol = (Roles)result.Get<int>("Rol");
            user.Password = result.Get<string>("Password")!;
            user.Email = result.Get<string>("Email")!;
            return user;
        }
    }

    /// <summary>
    /// Clase que representa un usuario.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Identificador del usuario.
        /// </summary>
        public int IdUser { get; set; }

        /// <summary>
        /// Nombre de usuario.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Rol del usuario.
        /// </summary>
        public Roles Rol { get; set; }

        /// <summary>
        /// Contraseña del usuario.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Correo electrónico del usuario.
        /// </summary>
        public string Email { get; set; }


        private const int factor = 7;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="User"/>.
        /// </summary>
        public User()
        {
            IdUser = 0;
            Username = "";
            Rol = 0;
            Password = "";
            Email = "";
        }

        /// <summary>
        /// Crea un hash BCrypt de la contraseña.
        /// </summary>
        /// <param name="password">La contraseña a hashear.</param>
        /// <exception cref="ArgumentOutOfRangeException">Lanzada cuando la contraseña es demasiado corta.</exception>
        public void CreateBCryptHash(string password)
        {
            if (password.Trim().Length < factor)
            {
                throw new ArgumentOutOfRangeException(nameof(password), "La contraseña debe ser de al menos " + factor + " caracteres.");
            }
            try
            {
                Password = BCrypt.Net.BCrypt.HashPassword(password.Trim(), factor);
            }
            catch (ArgumentOutOfRangeException)
            {
                Password = BCrypt.Net.BCrypt.HashPassword(password.Trim());
            }
        }

        /// <summary>
        /// Verifica si la contraseña dada coincide con la contraseña almacenada.
        /// </summary>
        /// <param name="pass">La contraseña a verificar.</param>
        /// <returns>True si la contraseña es correcta, de lo contrario false.</returns>
        public bool VerifyPassword(string pass)
        {
            try
            {
                return BCrypt.Net.BCrypt.Verify(pass, Password);
            }
            catch (SaltParseException)
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Excepción lanzada cuando un usuario no existe.
    /// </summary>
    [Serializable]
    public class UserNotExistException : UserManagerException
    {
        /// <summary>
        /// Crea una nueva instancia de la clase <see cref="UserNotExistException"/>.
        /// </summary>
        /// <param name="name">El nombre del usuario que no existe.</param>
        public UserNotExistException(string name)
            : base("Usuario '" + name + "' no existe")
        {
        }
    }

    /// <summary>
    /// Excepción lanzada cuando un usuario ya existe.
    /// </summary>
    [Serializable]
    public class UserExistsException : UserManagerException
    {
        /// <summary>
        /// Crea una nueva instancia de la clase <see cref="UserExistsException"/>.
        /// </summary>
        /// <param name="name">El nombre del usuario que ya existe.</param>
        public UserExistsException(string name)
            : base("Usuario '" + name + "' ya existe")
        {
        }
    }

    /// <summary>
    /// Excepción generada por el gestor de usuarios.
    /// </summary>
    [Serializable]
    public class UserManagerException : Exception
    {
        /// <summary>
        /// Crea una nueva instancia de la clase <see cref="UserManagerException"/>.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        public UserManagerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Crea una nueva instancia de la clase <see cref="UserManagerException"/> con una excepción interna.
        /// </summary>
        /// <param name="message">El mensaje para la excepción.</param>
        /// <param name="inner">La excepción interna.</param>
        public UserManagerException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
